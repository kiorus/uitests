package ru.emdev.uitests.core;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.testng.Assert;
import ru.emdev.uitests.core.utils.JavaScriptActions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class Module {

    private String moduleTitleOnThePage;

    protected Module(String moduleTitleOnThePage) {
        this.moduleTitleOnThePage = moduleTitleOnThePage;
    }

    protected String getModuleTitleOnThePage() {
        return moduleTitleOnThePage;
    }

    @Step("Запрос модуля на странице")
    protected SelenideElement getModuleOnThePage() {
        sleep(1000);
        switchTo().defaultContent();
        return $(byXpath("//section//*[text()='" + this.moduleTitleOnThePage + "']/ancestor::section[1]"));
    }

    @Step("Запрос ID инстанса")
    protected String getInstanceId() {
        String moduleInstanceId = getModuleOnThePage().getAttribute("id");

        Pattern pattern = Pattern.compile("INSTANCE_[A-Za-z0-9]+");
        Matcher matcher = pattern.matcher(moduleInstanceId);

        if (matcher.find()) {
            moduleInstanceId = matcher.group();
        } else {
            Assert.fail("Не получилось извлечь moduleInstanceId для модуля \"" + this.moduleTitleOnThePage + "\"");
        }
        return moduleInstanceId;
    }

    @Step("Открытие опций")
    protected void clickOpenOptions() {
        // Открывание hover через javascript, работает во всех браузерах
        JavaScriptActions.hoverThroughJavascript(getModuleOnThePage());
        sleep(1000);
        $(byXpath("//section[contains(@id,'" + getInstanceId() + "')]/header/menu/div[contains(@class,'portlet-options')]/a/span")).shouldBe(Condition.visible).click();
    }

    @Step("Удаление модуля")
    protected void clickDeleteModule() {
        $(byXpath("//li[contains(@class,'portlet-close')]/a[contains(@id,'" + getInstanceId() + "')]/span")).shouldBe(Condition.visible).click();
        confirm();
    }

    @Step("Открытие конфигурации")
    protected void clickOpenConfiguration() {
        $(byXpath("//li[contains(@class,'portlet-configuration')]/a[contains(@id,'" + getInstanceId() + "')]/span")).shouldBe(Condition.visible).click();
    }

    @Step("Открытие вкладки для выбора материала")
    protected void clickMaterialChoice() {
        SelenideElement navTabs = $(byXpath("//ul[contains(@class,'nav nav-underline')]")); //Набор "табов"
        SelenideElement materialChoiceTab = $(byXpath("//li[@data-tab-name='asset-selection']/a"));
        if (navTabs.exists()) {
            materialChoiceTab.shouldBe(Condition.visible).click();
        }
    }

    @Step("Открытие вкладки для настройки свойств")
    protected void clickDisplayProperties() {
        SelenideElement navTabs = $(byXpath("//ul[contains(@class,'nav nav-underline')]")); //Набор "табов"
        SelenideElement displayPropertiesTab = $(byXpath("//li[@data-tab-name='display-settings']/a"));
        if (navTabs.exists()) {
            displayPropertiesTab.shouldBe(Condition.visible).click();
        }
    }

    @Step("Сохранить конфигурацию")
    protected void clickSubmitConfiguration() {
        $(byXpath("//button[@type='submit']")).shouldBe(Condition.visible).click();
    }

    @Step("Закрыть уведомление о сохранении настроек")
    protected void closeSuccessMessage() {
        $(byXpath("//div[@id='alertContainer']//button")).shouldBe(Condition.visible).click();
    }

    @Step("Закрытие меню конфигурации")
    protected void clickCloseConfiguration() {
        $(byXpath("//button[contains(@class,'btn-cancel')]/span")).shouldBe(Condition.visible).click();
    }

    @Step("Закрытие меню конфигурации с использованием \"X\"")
    protected void clickCloseConfigurationByX() {
        switchTo().defaultContent();
        $(byXpath("//button[contains(@class,'close-content')]")).shouldBe(Condition.visible).click();
    }

}
