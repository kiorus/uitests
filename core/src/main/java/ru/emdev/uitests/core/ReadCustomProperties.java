package ru.emdev.uitests.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadCustomProperties {

    private Properties properties;

    public ReadCustomProperties(String pathToPropertiesFile) {
        properties = new Properties();
        try(InputStream inputStream = new FileInputStream(new File("").getAbsolutePath() + "/" + pathToPropertiesFile)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }

}
