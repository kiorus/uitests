package ru.emdev.uitests.core.portlets.favorites;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.testng.Assert;
import ru.emdev.uitests.core.Module;
import ru.emdev.uitests.core.portlets.Paginator;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static ru.emdev.uitests.core.Constants.OTHER_BUTTON_TYPE_ONWARD;

public class InKnowFavorites extends Module {

    public InKnowFavorites(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    private String moduleTitleOnThePage = getModuleTitleOnThePage();

    private Paginator Paginator = new Paginator(moduleTitleOnThePage);

    /*
    Проверка, что контент уже в "Избранном"
    */
    @Step("Проверка, что контент в \"Избранном\"")
    public boolean checkContentStatusInTheFavoriteById(String contentId) {
        sleep(1000);
        SelenideElement favoriteContent = $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//a[contains(@href,'_assetEntryId=" +
                contentId + "') and contains(@class,'iap-fav-ctrl_state_on')]"));

        switchTo().defaultContent();
        $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]")).shouldBe(Condition.visible);

        if (Paginator.presencePaginatorOnPage()) {
            while (Paginator.isButtonInPaginatorEnabled(OTHER_BUTTON_TYPE_ONWARD)){
                if (favoriteContent.exists()) {
                    break;
                }

                Paginator.clickPaginatorButtonIfEnabled(OTHER_BUTTON_TYPE_ONWARD);
            }

            return favoriteContent.exists();
        } else {

            return favoriteContent.exists();
        }
    }

    /*
    Удаление контента из "Избранного", через портлет "Избранное"
    */
    @Step("Удаление контента из \"Избранного\"")
    public void clickRemoveContentFromTheFavoriteById(String contentId) {
        switchTo().defaultContent();

        if (checkContentStatusInTheFavoriteById(contentId)) {
/*
 TODO По какой-то причине, кликая на "звёздочку" в портлете Избранное, средствами Selenide,
    клик происходит, но не приносит результата (в браузере Хром)
            $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//a[contains(@href,'_assetEntryId=" +
                    contentId + "')]/span[contains(@class,'iap-fav-ctrl__icon-remove')]")).click();
*/
            executeJavaScript("document.evaluate(\"//section[contains(@id,'" + super.getInstanceId() + "')]//a[contains(@href,'_assetEntryId=" +
                    contentId + "')]/span[contains(@class,'iap-fav-ctrl__icon-remove')]\",document,null,XPathResult.ANY_TYPE,null).iterateNext().click()");
            sleep(1000);
        } else {
            Assert.fail("Контент " + contentId + " не найден в Избранном");
        }
    }

}
