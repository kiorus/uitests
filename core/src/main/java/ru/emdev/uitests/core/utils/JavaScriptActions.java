package ru.emdev.uitests.core.utils;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.executeJavaScript;

public class JavaScriptActions {

    private JavaScriptActions() {
    }

    @Step("Скрол страницы")
    public static void scrollToPageOfTop() {
        executeJavaScript("window.scroll(0,0)");
    }

    @Step("Выключение всех всплывающих сообщений/окон")
    public static void disableAllTooltips() {
        executeJavaScript("$(document.head).append('<style>.tooltip {display: none;}</style>')");
    }

    @Step("Открывание hover через javascript")
    public static void hoverThroughJavascript(SelenideElement moduleOnThePage) {
        //работает во всех браузерах
        executeJavaScript("arguments[0].classList.add('open')", moduleOnThePage);
    }
}
