package ru.emdev.uitests.core.portlets.category.manager;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.confirm;

public class CategoryManagerRemoveFromArchiveCategoryDialog {

    private String getUnArchiveCategoryIconXPath(String vocabularyName, String categoryNameForUnArchiving) {
        return "//span[contains(text(),'" + vocabularyName + "')]/../../..//span[text()='" + categoryNameForUnArchiving +
                "']/..//span[@class='icon-upload-alt']";
    }

    protected void unArchiveCategoryWithContent(String vocabularyName, String categoryNameForUnArchiving) {
        //Нажать на иконку "Разархивации" категории (справа от названия категории)
        $(byXpath(getUnArchiveCategoryIconXPath(vocabularyName, categoryNameForUnArchiving))).shouldBe(Condition.visible).click();
        //Поставить галочку в чекбоксе "Никогда не устаревает"
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showUnarchCategoryDialog']//input[@ng-model='acmCtrl.categoryDialog.neverExpire']/../span/span"))
                .shouldBe(Condition.visible).click();
        //Нажать кнопку "Продолжить"
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showUnarchCategoryDialog']//clay-dialog-primary-button/../../button")).shouldBe(Condition.visible).click();
    }

    protected void unArchiveCategoryWithoutContent(String vocabularyName, String categoryNameForUnArchiving) {
        //Нажать на иконку "Разархивации" категории (справа от названия категории)
        $(byXpath(getUnArchiveCategoryIconXPath(vocabularyName, categoryNameForUnArchiving))).shouldBe(Condition.visible).click();
        confirm();
    }
}
