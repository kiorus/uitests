package ru.emdev.uitests.core.portlets.favorites;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.Module;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class InKnowFavoritesContent extends Module {

    public InKnowFavoritesContent(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    @Step("Запрос ID контента по имени")
    String getContentIdByName(String contentName) {
        String contentId  = "";
        SelenideElement contentElement = $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//a[contains(@href,'" + super.getInstanceId() + "') and text()='" +
                contentName + "']/../../span"));

        if (contentElement.exists()) {
            contentId = contentElement.getAttribute("id");
        }

        return contentId;
    }

    @Step("Запрос имени контента по ID")
    public String getContentNameById(String contentId) {
        return $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//a[contains(@href,'_assetEntryId=" + contentId + "')]/..")).text();
    }

    @Step("Запрос имен всех контентов")
    public ElementsCollection getAllContentNames() {
        return $$(byXpath("//span/a[contains(@href,'" + super.getInstanceId() + "')]"));
    }
}
