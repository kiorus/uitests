package ru.emdev.uitests.core.portlets;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.Module;
import ru.emdev.uitests.core.utils.JavaScriptActions;

import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class Paginator extends Module {

    public Paginator(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    @Step("Проверка что пагинатор есть в портлете")
    public boolean presencePaginatorOnPage() {
        return $(byXpath("//section[contains(@id,'"+ super.getInstanceId() +
                "')]//div[contains(@class,'lfr-pagination')]/ul/li/a")).exists();

    }

    @Step("Проверка что кнопка доступна для нажатия")
    public boolean isButtonInPaginatorEnabled(String buttonType) {
        switchTo().defaultContent();
        Allure.addAttachment("Искомая кнопка", buttonType);

        return !$(byXpath("//section[contains(@id,'"+ super.getInstanceId() +
                "')]//div[contains(@class,'lfr-pagination')]/ul/li[" +
                buttonType + "]/a")).getAttribute("href").equals("javascript:;");

    }

    @Step("Нажатие на кнопку в пагинаторе")
    public void clickPaginatorButtonIfEnabled(String buttonType) {
        if (isButtonInPaginatorEnabled (buttonType)) {
            SelenideElement button = $(byXpath("//section[contains(@id,'" + super.getInstanceId() +
                    "')]//div[contains(@class,'lfr-pagination')]/ul/li[" +
                    buttonType + "]/a"));
            JavaScriptActions.hoverThroughJavascript(super.getModuleOnThePage());
            button.click();
        }
    }
}
