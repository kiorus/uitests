package ru.emdev.uitests.core;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.emdev.uitests.core.utils.JavaScriptActions;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class RemoveModuleFromThePage extends Module {

    public RemoveModuleFromThePage(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
        switchTo().defaultContent();

        ElementsCollection modules = $$(byXpath("//section/header//*[text()='" + moduleTitleOnThePage + "']"));
        for (SelenideElement module : modules) {
            refresh();
            JavaScriptActions.scrollToPageOfTop();
            JavaScriptActions.disableAllTooltips();
            sleep(1000);

            super.clickOpenOptions();
            super.clickDeleteModule();
            sleep(1000);
        }
    }

}
