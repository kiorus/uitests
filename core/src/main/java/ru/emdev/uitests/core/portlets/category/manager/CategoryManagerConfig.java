package ru.emdev.uitests.core.portlets.category.manager;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.Constants;
import ru.emdev.uitests.core.Module;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class CategoryManagerConfig extends Module {

    public CategoryManagerConfig(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    /*
    Методы конфигурации портлета
    */
    @Step("Открытие конфигурации")
    public void openConfiguration() {
        switchTo().defaultContent();
        super.clickOpenOptions();
        super.clickOpenConfiguration();
        sleep(2000);
        switchTo().frame("_ru_emdev_inknowledge_categorymanager_portlet_AssetCategoryManagerPortlet_" +
                super.getInstanceId() + "_configurationIframeDialog_iframe_");
    }

    @Step("Нажатие на кнопку сохранить")
    public void clickSaveButton() {
        $(byXpath("//button[@type='submit']")).shouldBe(Condition.visible).click();
        sleep(1000);
    }

    /*
    Область действия
    */
    @Step("Область действия")
    public void changeScope(String scope) {
        SelenideElement scopeCollapse = $(byXpath("//a[@aria-controls='_com_liferay_portlet_configuration_web_portlet_" +
                "PortletConfigurationPortlet_emdev-ac-m-configuration-scopeContent']"));
        String scopeSelectorXPath = "//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_scope']";
        SelenideElement scopeSelector = $(byXpath(scopeSelectorXPath));
        SelenideElement scopeOption = $(byXpath(scopeSelectorXPath + "/option[@value='" + scope + "']"));
        String selectedScopeOptionXPath = scopeSelectorXPath + "/option[@selected]";
        SelenideElement selectedScopeOption = $(byXpath(selectedScopeOptionXPath));

        if (scopeCollapse.getAttribute("aria-expanded").equals("false")) {
            scopeCollapse.shouldBe(Condition.visible).click();
        }
        scopeOption.shouldBe(Condition.visible);
        if ($$(byXpath(selectedScopeOptionXPath)).size() > 0) {
            if (!selectedScopeOption.getAttribute("value").equals(scope)) {
                scopeSelector.shouldBe(Condition.visible).click();
                scopeOption.shouldBe(Condition.visible).click();
                super.closeSuccessMessage();
            }
        } else {
            if (!scope.equals(Constants.CATEGORY_MANAGER_CONFIG_OPTION_SCOPE_ALL)) {
                scopeSelector.shouldBe(Condition.visible).click();
                scopeOption.shouldBe(Condition.visible).click();
                super.closeSuccessMessage();
            }
        }
    }

    /*
    Отображаемые словари
    */

    @Step("Отображение словаря")
    public void changeVocabularies(String vocabulary) {
        SelenideElement vocabulariesCollapse = $(byXpath("//a[@aria-controls='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet" +
                "_emdev-ac-m-configuration-vocabulariesContent']"));
        SelenideElement allVocabulariesSelector = $(byXpath("//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_allVocabularies']"));
        SelenideElement allVocabularyOption = $(byXpath("//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_allVocabularies']" +
                "/option[@value='" + vocabulary + "']"));

        if (vocabulariesCollapse.getAttribute("aria-expanded").equals("false")) {
            vocabulariesCollapse.shouldBe(Condition.visible).click();
        }
        allVocabulariesSelector.shouldBe(Condition.visible).click();
        allVocabularyOption.shouldBe(Condition.visible).click();
    }

    /*
    Отображение
    */
    @Step("Развернуть \"Показать коллапс\"")
    private void expandDisplayCollapse() {
        SelenideElement displayCollapse = $(byXpath("//a[@aria-controls='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_" +
                "emdev-ac-m-configuration-displayContent']"));

        if (displayCollapse.getAttribute("aria-expanded").equals("false")) {
            displayCollapse.shouldBe(Condition.visible).click();
        }
    }

    @Step("Изменение отображения")
    public void changeDisplay(String display) {
        SelenideElement displaySelector = $(byXpath("//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_displayType']"));
        SelenideElement displayOption = $(byXpath("//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_displayType']" +
                "/option[@value='" + display + "']"));

        expandDisplayCollapse();
        displaySelector.shouldBe(Condition.visible).click();
        displayOption.shouldBe(Condition.visible).click();
    }

    @Step("Включение режима редактирования")
    public void enableEditMode() {
        SelenideElement displayCheckbox = $(byXpath("//input[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_editMode']"));

        expandDisplayCollapse();
        if (!displayCheckbox.shouldBe(Condition.visible).is(Condition.selected)) {
            displayCheckbox.shouldBe(Condition.visible).setSelected(true);
        }
    }

    @Step("Включение \"Назначить ответственного\"")
    public void enableAppointResponsible() {
        SelenideElement appointCheckbox = $(byXpath("//input[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_appointResponsible']"));

        expandDisplayCollapse();
        if (!appointCheckbox.shouldBe(Condition.visible).is(Condition.selected)) {
            appointCheckbox.shouldBe(Condition.visible).setSelected(true);
        }
    }

}
