package ru.emdev.uitests.core.utils.ssh;

import com.jcraft.jsch.JSchException;
import ru.emdev.uitests.core.ReadCustomProperties;

public class ReverseSSHPortForwarding extends SSHSession {

    public ReverseSSHPortForwarding(ReadCustomProperties sshPropertiesFile, boolean authByPrivateKey) {
        super(sshPropertiesFile,authByPrivateKey);
    }

    public void open() throws JSchException {
        super.createSession();
        super.getSSHSession().setPortForwardingR(super.getSSHRemotePort(), super.getSSHLocalHost(), super.getSSHLocalPort());
    }

    public void close() throws JSchException {
        super.closeSession();
    }
}
