package ru.emdev.uitests.core.portlets.category.manager;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.utils.JavaScriptActions;
import ru.emdev.uitests.core.Module;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class CategoryManagerActions extends Module {

    private String vocabularyName;
    private CategoryManagerAddVocabularyAndCategoryDialog addDialog = new CategoryManagerAddVocabularyAndCategoryDialog();
    private CategoryManagerRemoveVocabularyAndCategoryDialog removeDialog = new CategoryManagerRemoveVocabularyAndCategoryDialog();
    private CategoryManagerAddToArchiveCategoryDialog archiveDialog = new CategoryManagerAddToArchiveCategoryDialog();
    private CategoryManagerRemoveFromArchiveCategoryDialog unArchiveDialog = new CategoryManagerRemoveFromArchiveCategoryDialog();

    public CategoryManagerActions(String moduleTitleOnThePage, String vocabularyName) {
        super(moduleTitleOnThePage);
        this.vocabularyName = vocabularyName;
    }

    @Step("Закрыть окно подтверждения успешного действия")
    private void closeCategoryManagerSuccessMessage() {
        sleep(1000);
        $(byXpath("//simple-alert/div/button")).shouldBe(Condition.visible).click();
    }

    /*
    Работа со словарями
    */
    @Step("Добавление словаря")
    public void addVocabulary(String vocabularyDescription, boolean isPublic) {
        $(byXpath("//section[contains(@id,'_ru_emdev_inknowledge_categorymanager_portlet_AssetCategoryManagerPortlet')]" +
                "//span[@ng-click='acmCtrl.openCreateVocabularyDialog()']")).shouldBe(Condition.visible).click();
        sleep(1000);
        JavaScriptActions.scrollToPageOfTop();
        addDialog.typeName(this.vocabularyName, false);
        sleep(1000);
        addDialog.typeDescription(vocabularyDescription, false);
        sleep(1000);
        if (isPublic) {
            addDialog.setIsPublicCheckbox();
        }
        addDialog.clickSaveVocabularyButton();
        closeCategoryManagerSuccessMessage();
        sleep(1000);
    }

    @Step("Удаление словаря")
    private void removeVocabulary() {
        $(byXpath("//span[contains(text(),'" + this.vocabularyName + "')]/..//span[@class='icon-minus-sign-alt']")).shouldBe(Condition.visible).click();
    }

    @Step("Удаление словаря с перемещением категорий")
    public void removeVocabularyWithReplaceCategories(String newVocabulary) {
        switchTo().defaultContent();
        removeVocabulary();
        sleep(1000);
        removeDialog.choiceNewVocabularyToReplaceCategories(newVocabulary);
        sleep(1000);
        removeDialog.confirmRemoveVocabulary();
        closeCategoryManagerSuccessMessage();
    }

    @Step("Удаление словаря со всеми категориями")
    public void removeVocabularyWithAllCategories() {
        switchTo().defaultContent();
        removeVocabulary();
        sleep(1000);
        removeDialog.enableRemoveVocabularyWithAllCategory();
        sleep(1000);
        removeDialog.confirmRemoveVocabulary();
        closeCategoryManagerSuccessMessage();
    }

/*
    TODO Доработать механизм идентификации Словарей и Категорий так, чтобы тесты не падали если есть несколько
        вхождений искомого слова в названии Словарей или Категорий
        Проблемный кусок кода: //span[contains(text(),'" + this.vocabularyName + "')]
        !!! Если обеспечить уникальность имён/названий контентов, то данноую проблему можно избежать. !!!
*/

    // при редактировании Словаря используются теже методы, что и при создании (typeName,typeDescription,setIsPublicCheckbox ...)
    @Step("Редактирования словаря")
    public void editVocabulary(String newName, String newDescription, boolean isPublic) {
        $(byXpath("//span[contains(text(),'" + this.vocabularyName + "')]/..//span[@class='icon-edit']")).shouldBe(Condition.visible).click();
        sleep(1000);
        JavaScriptActions.scrollToPageOfTop();
        this.vocabularyName = newName;
        addDialog.typeName(newName, false);
        sleep(1000);
        addDialog.typeDescription(newDescription, false);
        sleep(1000);
        if (isPublic) {
            addDialog.setIsPublicCheckbox();
        } else {
            addDialog.unsetIsPublicCheckbox();
        }
        addDialog.clickSaveVocabularyButton();
        closeCategoryManagerSuccessMessage();
    }

    /*
    Работа с категориями
    */
    private void clickIconPlusSign(String name) {
        $(byXpath("//span[contains(text(),'" + name + "')]/..//span[@class='icon-plus-sign-2']")).shouldBe(Condition.visible).click();
    }

    private void addCategoryActions(String categoryName, String categoryDescription, String responsibles) {
        sleep(1000);
        JavaScriptActions.scrollToPageOfTop();
        addDialog.typeName(categoryName, true);
        sleep(1000);
        addDialog.typeDescription(categoryDescription, true);
        sleep(1000);
        if (!responsibles.equals("")) {
            addDialog.typeResponsibles(responsibles);
        }
        addDialog.clickSaveCategoryButton();
        closeCategoryManagerSuccessMessage();
        sleep(1000);
    }

    public void addCategory(String categoryName, String categoryDescription, String responsibles) {
        clickIconPlusSign(this.vocabularyName);
        addCategoryActions(categoryName,categoryDescription,responsibles);
    }

    public void addCategory(String parentCategoryName, String categoryName, String categoryDescription, String responsibles) {
        clickIconPlusSign(parentCategoryName);
        addCategoryActions(categoryName,categoryDescription,responsibles);
    }

    @Step("Генерация XPath для категории")
    private String removeCategoryXPath(String categoryName) {
        return "//span[contains(text(),'" + this.vocabularyName + "')]/../../..//span[text()='" + categoryName +
                "']/..//span[@class='icon-minus-sign-alt']";
    }

    @Step("Развернуть свётнутый словарь")
    private void collapsedVocabulary() {
        SelenideElement vocabularySection = $(byXpath("//span[contains(text(),'" + this.vocabularyName + "')]/../../.."));
        vocabularySection.shouldBe(Condition.visible);
        boolean isCollapsedVocabulary = executeJavaScript("return arguments[0].classList.contains('collapsed')", vocabularySection);
        if (isCollapsedVocabulary) {
            $(byXpath("//span[contains(text(),'" + this.vocabularyName + "')]")).shouldBe(Condition.visible).shouldBe(Condition.visible).click();
        }
    }

    @Step("Удаление пустых категорий")
    public void removeEmptyCategory(String emptyCategoryName) {
        switchTo().defaultContent();
        collapsedVocabulary();
        $(byXpath(removeCategoryXPath(emptyCategoryName))).shouldBe(Condition.visible).click();
        removeDialog.confirmRemoveEmptyCategory();
        closeCategoryManagerSuccessMessage();
    }

    @Step("Удалиение категории и перемещение содержимое")
    public void removeCategoryAndReplaceContent(String removeCategoryName, String newCategoryName) {
        switchTo().defaultContent();
        collapsedVocabulary();
        $(byXpath(removeCategoryXPath(removeCategoryName))).shouldBe(Condition.visible).click();
        removeDialog.confirmRemoveCategoryAndReplaceContent(newCategoryName);
        closeCategoryManagerSuccessMessage();
        sleep(1000);
    }

    @Step("Редактирование категории")
    public void editCategory(String oldCategoryName, String newCategoryName, String newDescription, String responsibles) {
        switchTo().defaultContent();
        collapsedVocabulary();
        $(byXpath("//span[contains(text(),'" + this.vocabularyName + "')]/../../..//span[text()='" + oldCategoryName +
                "']/..//span[@class='icon-edit']")).shouldBe(Condition.visible).click();
        sleep(1000);
        JavaScriptActions.scrollToPageOfTop();
        addDialog.typeName(newCategoryName, true);
        sleep(1000);
        addDialog.typeDescription(newDescription, true);
        sleep(1000);
        if (!responsibles.equals("")) {
            addDialog.typeResponsibles(responsibles);
        }
        addDialog.clickSaveCategoryButton();
        closeCategoryManagerSuccessMessage();
    }

    @Step("Архивация категории {categoryNameForArchiving} со всем контентом")
    public void archiveCategoryWithContent(String categoryNameForArchiving) {
        switchTo().defaultContent();
        JavaScriptActions.scrollToPageOfTop();
        collapsedVocabulary();
        archiveDialog.addToArchiveWithContent(this.vocabularyName, categoryNameForArchiving);
        closeCategoryManagerSuccessMessage();
    }

    @Step("Архивация категории {categoryName} с перемещением контента в категорию {newCategoryNameForContent}")
    public void archiveCategoryWithReplaceContent(String categoryNameForArchiving, String newCategoryNameForContent) {
        switchTo().defaultContent();
        JavaScriptActions.scrollToPageOfTop();
        collapsedVocabulary();
        archiveDialog.addToArchiveWithReplaceContent(this.vocabularyName, categoryNameForArchiving, newCategoryNameForContent);
        closeCategoryManagerSuccessMessage();
    }

    @Step("Проверка, заархивирована категория {categoryName} или нет")
    public void isCategoryArchived(String categoryName) {
        /*
        TODO Сделать проверку - находится ли категория в архиве или нет
            Нужна ли такая проверка ???
         */
    }

    @Step("Разархивация категории {categoryNameForUnArchiving}")
    public void unArchiveCategory(String categoryNameForUnArchiving) {
        switchTo().defaultContent();
        JavaScriptActions.scrollToPageOfTop();
        collapsedVocabulary();
        unArchiveDialog.unArchiveCategoryWithContent(this.vocabularyName, categoryNameForUnArchiving);
        closeCategoryManagerSuccessMessage();
    }
}
