package ru.emdev.uitests.core.utils.mail;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class SmtpServer {

    public static final int CUSTOM_SMTP_PORT = 2526;

    private SimpleSmtpServer server;

    public void startSmtpServer(int port) throws Exception {
        server = SimpleSmtpServer.start(port);
    }

    public void stopSmtpServer() {
        server.stop();
    }

    public List<SmtpMessage> getReceivedEmails() {
        return server.getReceivedEmails();
    }

    public void resetEmails() {
        server.reset();
    }

    private MimeMessage createMessage(Session session, String from, String to, String subject, String body) throws MessagingException {
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(from));
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        msg.setText(body);
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
        return msg;
    }

    private Properties getMailProperties() {
        Properties mailProps = new Properties();
        mailProps.setProperty("mail.smtp.host", "localhost");
        mailProps.setProperty("mail.smtp.port", "" + server.getPort());
        mailProps.setProperty("mail.smtp.sendpartial", "true");
        return mailProps;
    }

    public void sendMessage(String from, String to, String subject, String body) throws MessagingException {
        Properties mailProps = getMailProperties();
        Session session = Session.getInstance(mailProps, null);
        //session.setDebug(true);

        MimeMessage msg = createMessage(session, from, to, subject, body);
        Transport.send(msg);
    }
}
