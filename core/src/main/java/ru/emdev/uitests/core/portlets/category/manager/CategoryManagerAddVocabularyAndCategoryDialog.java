package ru.emdev.uitests.core.portlets.category.manager;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;

public final class CategoryManagerAddVocabularyAndCategoryDialog {

    private SelenideElement isPublicCheckbox = $(byXpath("//input[@ng-model='acmCtrl.vocabularyDialog.vocabularyData.isPublic']"));

    protected CategoryManagerAddVocabularyAndCategoryDialog() {
    }

    @Step("Ввод значения в поле формы")
    private String getFormType(boolean isCategory) {
        return isCategory ? "acmCtrl.categoryForm" : "acmCtrl.vocabularyForm";
    }

    @Step("Ввод в поле типа Имя")
    protected void typeName(String name, boolean isCategory) {
        $(byXpath("//form[@name='" + getFormType(isCategory) + "']//span[@id='_com_liferay_journal_web_portlet_JournalPortlet_descriptionBoundingBox']/input"))
                .shouldBe(Condition.visible).setValue(name);
    }

    @Step("Ввод в поле типа Описание")
    protected void typeDescription(String description, boolean isCategory) {
        $(byXpath("//form[@name='" + getFormType(isCategory) + "']//span[@id='_com_liferay_journal_web_portlet_JournalPortlet_descriptionBoundingBox']/textarea"))
                .shouldBe(Condition.visible).setValue(description);
    }

    @Step("Ввод значения в поле типа Ответственный")
    protected void typeResponsibles(String responsible) {
        $(byXpath("//users-selector//input")).shouldBe(Condition.visible).setValue(responsible);
    }

    @Step("Включение флага Общедоступный")
    protected void setIsPublicCheckbox() {
        if (!isPublicCheckbox.is(Condition.selected)) {
            executeJavaScript("arguments[0].checked = true", isPublicCheckbox);
        }
    }

    @Step("Выключение флага Общедоступный")
    protected void unsetIsPublicCheckbox() {
        if (isPublicCheckbox.is(Condition.selected)) {
            executeJavaScript("arguments[0].checked = false", isPublicCheckbox);
        }
    }

    @Step("Сохранение категории")
    protected void clickSaveCategoryButton() {
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showAddModCategoryDialog']//button[@class='btn btn-primary']")).shouldBe(Condition.visible).click();
    }

    @Step("Сохранение словаря")
    protected void clickSaveVocabularyButton() {
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showAddModVocabularyDialog']//button[@class='btn btn-primary']")).shouldBe(Condition.visible).click();
    }

}


