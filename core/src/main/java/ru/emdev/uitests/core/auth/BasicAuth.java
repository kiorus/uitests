package ru.emdev.uitests.core.auth;

import static com.codeborne.selenide.Selenide.open;

import io.qameta.allure.Step;
import ru.emdev.uitests.core.ReadCustomProperties;

public class BasicAuth {

    private static final String PROP_BASIC_AUTH_URL = "basicAuthUrl";
    private static final String PROP_BASIC_AUTH_LOGIN = "basicAuthLogin";
    private static final String PROP_BASIC_AUTH_PASSWORD = "basicAuthPassword";

    private ReadCustomProperties basicAuthProperties;

    public BasicAuth(String basicAuthPropFile) {
        basicAuthProperties = new ReadCustomProperties(basicAuthPropFile);
    }

    @Step("Открытие и Basic-авторизация на портале")
    public void runBasicAuthorization() {
        open(basicAuthProperties.readProperty(PROP_BASIC_AUTH_URL),
                "",
                basicAuthProperties.readProperty(PROP_BASIC_AUTH_LOGIN),
                basicAuthProperties.readProperty(PROP_BASIC_AUTH_PASSWORD));
    }

    @Step("Передаем адрес ссылки - " + PROP_BASIC_AUTH_URL)
    public String getBasicAuthUrl() {
        return basicAuthProperties.readProperty(PROP_BASIC_AUTH_URL);
    }

}
