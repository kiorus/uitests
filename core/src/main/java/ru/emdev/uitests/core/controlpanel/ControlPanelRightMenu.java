package ru.emdev.uitests.core.controlpanel;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ControlPanelRightMenu {

    private SelenideElement plusButton = $(byXpath("//a[@id='_com_liferay_product_navigation_control_menu_web_portlet_ProductNavigationControlMenuPortlet_addToggleId']/span"));
    private SelenideElement rootMenu; // Widgets,Content ...
    private SelenideElement parentMenu; // InKnowledge,News,Sample ...
    private ElementsCollection parentMenuItems; // Favorites,Asset Publisher ...
    private String parentMenuName;

    protected ControlPanelRightMenu() {
    }

    public ControlPanelRightMenu(String rootMenuName, String parentMenuName) {
        rootMenu = $(byXpath("//span[contains(text(),'" + rootMenuName + "')]/.."));
        parentMenu = $(byXpath("//div/a[contains(text(),'" + parentMenuName + "')]"));
        parentMenuItems = $$(byXpath("//div/a[contains(text(),'" + parentMenuName + "')]/../div/div/ul/li/span/span"));
        this.parentMenuName = parentMenuName;
    }

    protected String getParentMenuName() {
        return parentMenuName;
    }

    @Step("Нажатие на кнопку добавления \"+\"")
    public void clickPlusButton() {
        if (!$(byXpath("//div[@class='sidebar-header']/span[text()='Добавить']")).is(Condition.visible)) {
            plusButton.click();
        }
    }

    @Step("Нажатие на корневое меню")
    public void clickRootMenu() {
        if (!rootMenu.getAttribute("aria-expanded").equals("true")) {
            rootMenu.click();
        }
    }

    @Step("Нажатие на кнопку меню")
    public void clickParentMenu() {
        parentMenu.click();
        if (!parentMenu.getAttribute("aria-expanded").equals("true")) {
            parentMenu.click();
        }
    }

    @Step("Проверка наличия пункта в меню")
    public boolean checkSubMenuItem(String subMenuItemName) {
        boolean status = false;
        for (SelenideElement parentMenuItem : parentMenuItems) {
            if (parentMenuItem.getText().equals(subMenuItemName)) {
                status = true;
                break;
            }
        }
        return status;
    }

}
