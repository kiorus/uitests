package ru.emdev.uitests.core.portlets.category.manager;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public final class CategoryManagerRemoveVocabularyAndCategoryDialog {

    private final String vocabularyDialogXPath = "//clay-dialog[@trigger='acmCtrl.showDelVocabularyDialog']";

    @Step("Потдверждение удаление словаря")
    protected void confirmRemoveVocabulary() {
        $(byXpath(vocabularyDialogXPath + "//button[contains(@class,'btn-primary')]")).shouldBe(Condition.visible).click();
    }

    @Step("Разрешение удаления словаря со всеми категориями")
    protected void enableRemoveVocabularyWithAllCategory() {
        String removeVocabularyWithAllCategoryXPath = "//input[@ng-model='acmCtrl.vocabularyDialog.moveCategories' and @ng-value='false']";
        boolean isDisabled = $(byXpath(removeVocabularyWithAllCategoryXPath)).is(Condition.attribute("disabled"));
        if (!isDisabled) {
            $(byXpath(removeVocabularyWithAllCategoryXPath + "/../span")).shouldBe(Condition.visible).click();
        }
    }

    @Step("Выбор нового словаря для замены категорий")
    protected void choiceNewVocabularyToReplaceCategories(String newVocabulary) {
        String selectVocabularyXPath = vocabularyDialogXPath + "//select";
        $(byXpath(selectVocabularyXPath)).shouldBe(Condition.visible).click();
        $(byXpath(selectVocabularyXPath + "/option[@label='" + newVocabulary + "']")).shouldBe(Condition.visible).click();
    }

    @Step("Подтверждение удаления пустой категории")
    protected void confirmRemoveEmptyCategory() {
        $(byXpath("//clay-Dialog[@trigger='acmCtrl.showDelSimpleCategoryDialog']//button[contains(@class,'btn-primary')]")).shouldBe(Condition.visible).click();
    }

    @Step("Подтверждение удаления категории и замену содержимого")
    protected void confirmRemoveCategoryAndReplaceContent(String categoryName) {
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showDelCategoryDialog']//input[@ng-model='acmCtrl.categoryDialog.moveObjects' and @ng-value='true']/../span"))
                .shouldBe(Condition.visible).click();
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showDelCategoryDialog']//button[@ng-click='openDialog()']")).shouldBe(Condition.visible).click();
        $(byXpath("//clay-dialog[@trigger='showDialog'][1]//input[@ng-model='searchStrContainer.searchStr']")).shouldBe(Condition.visible).setValue(categoryName);
        $(byXpath("//tr[@ng-repeat='category in categoriesList']//span[text()='" + categoryName + "']")).shouldBe(Condition.visible).click();
        $(byXpath("//clay-dialog[@trigger='showDialog'][1]//clay-dialog-primary-button/../../button")).shouldBe(Condition.visible).click();
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showDelCategoryDialog']//clay-dialog-primary-button/../../button")).shouldBe(Condition.visible).click();
    }

}
