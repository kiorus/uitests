package ru.emdev.uitests.core;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class CheckModule {

    private String moduleName;
    private SelenideElement moduleStatusElement = $(byXpath("//ul/li[@data-qa-id='row']/*/*/*/strong[contains(text(),'Статус:')]/.."));
    private ElementsCollection modulesList = $$(byXpath("//ul/li/div/h2"));
    private boolean moduleStatus = false;

    public CheckModule(String moduleName) {
        this.moduleName = moduleName;
    }

    private void searchModule() {
        open("/group/control_panel/manage?_com_liferay_marketplace_app_manager_web_portlet_MarketplaceAppManagerPortlet_keywords=" +
                moduleName + "&p_p_mode=view&p_p_state=maximized" +
                "&p_p_id=com_liferay_marketplace_app_manager_web_portlet_MarketplaceAppManagerPortlet" +
                "&p_p_lifecycle=0&_com_liferay_marketplace_app_manager_web_portlet_MarketplaceAppManagerPortlet_mvcPath=%2Fview_search_results.jsp");
    }

    private void searchDuplicatesOfModule() {
        modulesList.shouldHaveSize(1);
    }

    public void checkModuleStatus() {
        searchModule();
        searchDuplicatesOfModule();
        if (moduleStatusElement.getText().equals("Статус: Активен")) {
            moduleStatus = true;
        }
    }

    public boolean getModuleStatus() {
        return moduleStatus;
    }

}
