package ru.emdev.uitests.core;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.controlpanel.ControlPanelRightMenu;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class AddModuleToThePage extends ControlPanelRightMenu {

    @Deprecated
    protected AddModuleToThePage(String rootMenuName, String parentMenuName, String subMenuItemName) {
        super(rootMenuName, parentMenuName);
        addModuleToPage(subMenuItemName);
    }

    protected AddModuleToThePage(String rootMenuName, String parentMenuName, String subMenuItemName, String columnId) {
        super(rootMenuName, parentMenuName);
        addModuleToPage(subMenuItemName,columnId);
        sleep(1000);
    }

    @Deprecated
    private void addModuleToPage(String subMenuItemName) {
//        SelenideElement dropZone = $(byXpath("//div[@id='layout-column_column-1']"));
//        SelenideElement controlPanelRightForm = $(byXpath("//form[@id='_com_liferay_product_navigation_control_menu_web_portlet_ProductNavigationControlMenuPortlet_addApplicationForm']"));
//        SelenideElement addModuleToPageButton = $(byXpath("//span[contains(@class,'drag-content-item') and @data-title='" + subMenuItemName + "']"));
//        executeJavaScript("arguments[0].scrollIntoView(arguments[1])",controlPanelRightForm,addModuleToPageButton);
//        addModuleToPageButton.dragAndDropTo(dropZone);
        addModuleToPage(subMenuItemName,Constants.LAYOUT_COLUMN_1);
    }

    @Step("Добавление модуля на страницу")
    private void addModuleToPage(String subMenuItemName, String columnId) {
        SelenideElement dropZone = $(byXpath(columnId));
        SelenideElement controlPanelRightForm = $(byXpath("//form[@id='_com_liferay_product_navigation_control_menu_web_portlet_ProductNavigationControlMenuPortlet_addApplicationForm']"));
        SelenideElement addModuleToPageButton = $(byXpath("//span[contains(@class,'drag-content-item') and @data-title='" + subMenuItemName + "']"));
        executeJavaScript("arguments[0].scrollIntoView(arguments[1])",controlPanelRightForm,addModuleToPageButton);
        addModuleToPageButton.dragAndDropTo(dropZone);
    }

}
