package ru.emdev.uitests.core.controlpanel;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class ControlPanelJournalPortletPage {

    @Step("Открытие страницы")
    public void openPage(String testPage) {
        switchTo().defaultContent();
        open(testPage + "/../~/control_panel/manage?p_p_id=com_liferay_journal_web_portlet_JournalPortlet&p_p_lifecycle=0" +
                "&p_p_state=maximized&p_p_mode=view&_com_liferay_journal_web_portlet_JournalPortlet_folderId=0" +
                "&_com_liferay_journal_web_portlet_JournalPortlet_orderByCol=modified-date" +
                "&_com_liferay_journal_web_portlet_JournalPortlet_orderByType=desc");
    }

    @Step("Закрыть окно подтверждение")
    private void clickCloseSubmitMessage() {
        sleep(1000);
        $(byXpath("//div[@id='alertContainer']//button")).shouldBe(Condition.visible).click();
    }

    @Step("Нажать на кнопку добавления контента")
    private void clickAddContentButton() {
        sleep(1000);
        $(byXpath("//li[@class='nav-item']/div/button")).shouldBe(Condition.visible).click();
    }

    @Step("Добавить артикул Inknowlege")
    public void addInknowledgeAtricle() {
        clickAddContentButton();
        $(byXpath("//li/a[text()='Статья InKnowledge']")).shouldBe(Condition.visible).click();
    }

    @Step("Добавить новость")
    public void addNews() {
        clickAddContentButton();
        $(byXpath("//li/a[text()='Новость']")).shouldBe(Condition.visible).click();
    }

    @Step("Открыть меню редактора контента")
    private void openEditContentMenu(String contentName) {
        $(byXpath("//li[@data-title='" + contentName + "']//div/button")).shouldBe(Condition.visible).click();
    }

    @Step("Получение ID меню контента")
    private String getEditContentMenuId(String contentName) {
        return $(byXpath("//li[@data-title='" + contentName + "']/div/div/label/input")).getAttribute("value");
    }

    @Step("Изменение контента")
    public void editContent(String contentName) {
        openEditContentMenu(contentName);
        $(byXpath("//div[@class='dropdown-menu show']/ul/li[contains(@data-deleteurl,'Id=" + getEditContentMenuId(contentName) + "')]/../li[1]/a"))
                .shouldBe(Condition.visible).click();
        sleep(2000);
    }

    @Step("Добавление контент в архив")
    public void makeArchiveOfContent(String contentName) {
        openEditContentMenu(contentName);
        $(byXpath("//div[@class='dropdown-menu show']/ul/li[contains(@data-expireurl,'Id=" +
                getEditContentMenuId(contentName) + "')]/button")).shouldBe(Condition.visible).click();
        clickCloseSubmitMessage();
    }

}
