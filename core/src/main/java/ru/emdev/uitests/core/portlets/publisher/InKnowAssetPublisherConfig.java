package ru.emdev.uitests.core.portlets.publisher;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.Module;

import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class InKnowAssetPublisherConfig extends Module {

    private SelenideElement addToFavoriteCheckbox = $(byXpath("//input[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_enableFavorites']"));
    private SelenideElement showDraftsCheckbox = $(byXpath("//input[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_enableViewDraft']"));
    private SelenideElement showArchivesCheckbox = $(byXpath("//input[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_enableViewArchive']"));

    public InKnowAssetPublisherConfig(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    /*
    Общие методы конфигурации портлета
    */
    @Step("Открытие конфигурации")
    public void openConfiguration() {
        switchTo().defaultContent();
        super.clickOpenOptions();
        super.clickOpenConfiguration();
        sleep(2000);
        switchTo().frame(0);
    }

    @Step("Включение контента")
    private void setAndEnableContent() {
        SelenideElement element = $(byXpath("//div[@id='setAndEnableTitle']/a[@aria-controls='setAndEnableContent']"));
        if (element.getAttribute("aria-expanded").equals("false")) {
            element.shouldBe(Condition.visible).click();
        }
        sleep(1000);
    }

    @Step("Нажатие на кнопку закрытия конфигурации")
    public void clickSubmitButtonAndCloseConfiguration() {
        super.clickSubmitConfiguration();
//        super.closeSuccessMessage();
//        super.clickCloseConfiguration();
    }

    /*
    Включение/Выключение избранного
    */
    @Step("Включение избранного")
    public void enableAddToFavoriteFunction() {
        super.clickDisplayProperties();
        setAndEnableContent();
        if (!addToFavoriteCheckbox.is(Condition.selected)) {
            addToFavoriteCheckbox.scrollTo();
            addToFavoriteCheckbox.setSelected(true);
        }
    }

    @Step("Выключение избранного")
    public void disableAddToFavoriteFunction() {
        super.clickDisplayProperties();
        setAndEnableContent();
        if (addToFavoriteCheckbox.is(Condition.selected)) {
            addToFavoriteCheckbox.scrollTo();
            addToFavoriteCheckbox.setSelected(false);
        }
    }

    /*
    Включение/Выключение черновиков и архивов
    */
    @Step("Включение черновиков и архивов")
    public void enableDrafts() {
        super.clickDisplayProperties();
        setAndEnableContent();
        if (!showDraftsCheckbox.is(Condition.selected)) {
            showDraftsCheckbox.scrollTo();
            showDraftsCheckbox.setSelected(true);
        }
    }

    @Step("Выключение черновиков и архивов")
    public void enableArchives() {
        super.clickDisplayProperties();
        setAndEnableContent();
        if (!showArchivesCheckbox.is(Condition.selected)) {
            showArchivesCheckbox.scrollTo();
            showArchivesCheckbox.setSelected(true);
        }
    }

    /*
    Включение метаданных
    */
    @Step("Включение метаданных")
    private void metadataContentUncollapse() {
        SelenideElement element = $(byXpath("//div[@id='metadataTitle']/a"));
        if (element.getAttribute("aria-expanded").equals("false")) {
            element.shouldBe(Condition.visible).click();
        }
    }

    @Step("Добавление метаданных")
    public void addMetadataContent(String availableMetadataField) {
        super.clickDisplayProperties();
        metadataContentUncollapse();
        String currentOptionXPath = "//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_currentMetadataFields']";
        $(byXpath(currentOptionXPath)).shouldBe(Condition.visible);
        boolean optionIsCurrent = $(byXpath(currentOptionXPath + "/option[@value='" + availableMetadataField + "']")).exists();
        if (!optionIsCurrent) {
            $(byXpath("//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_availableMetadataFields']"))
                    .selectOptionByValue(availableMetadataField);
            $(byXpath("//div[@id='metadataContent']//button[contains(@class,'move-left')]")).shouldBe(Condition.visible).click();
        }
    }

    /*
    Включение опции "Установить как публикатор по умолчанию"
     */
    @Step("Включение опции \"Установить как публикатор по умолчанию\"")
    public void makePublisherDefault () {
        SelenideElement input = $(byXpath("//input[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_defaultAssetPublisher']"));
        input.shouldBe(Condition.visible);
        sleep(500);

        if (!input.is(Condition.selected)) {
            input.click();
        }
    }

    @Step("изменяем статус пагинатора")
    public void paginatorStatusChange (String optionType) {
        super.clickDisplayProperties();

        SelenideElement dropDownMenu = $(byXpath("//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_paginationType']"));
        SelenideElement dropDownMenuOptions = $(byXpath(
                "//select[@id='_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_paginationType']/option[" +
                        optionType +"]"));

        dropDownMenu.click();
        dropDownMenuOptions.click();
        Allure.addAttachment("Параметр пагинации переключен. Новое значение", optionType);
    }
}
