package ru.emdev.uitests.core.portlets.publisher;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.testng.Assert;
import ru.emdev.uitests.core.Module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class InKnowAssetPublisherDraftAndArchive extends Module {

    public InKnowAssetPublisherDraftAndArchive(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    @Step("Нажать на чек-бокс \"Черновики\" в публикторе")
    public void clickDraftCheckboxOnThePage() {
        $(byXpath("//input[contains(@data-action-url,'" + super.getInstanceId() + "') and @class='iap-filters__view-drafts']/.."))
                .shouldBe(Condition.visible).click();
        sleep(1000);
    }

    @Step("Нажать на чек-бокс \"Архив\" в публикторе")
    public void clickArchiveCheckboxOnThePage() {
        $(byXpath("//input[contains(@data-action-url,'" + super.getInstanceId() + "') and @class='iap-filters__view-archive']/.."))
                .shouldBe(Condition.visible).click();
        sleep(1000);
    }

    @Step("Запрос всех статусов по имени контента")
    private ElementsCollection getAllStatusesByName(String contentName) {
        ElementsCollection allStatuses;
        ElementsCollection allStatusesViewAsAnnotation = $$(byXpath("//a[contains(text(),'" + contentName + "')]/../../strong"));
        ElementsCollection allStatusesViewAsTable = $$(byXpath("//a[contains(text(),'" + contentName + "')]/../div/strong"));
        ElementsCollection allStatusesViewAsAnnotationOfNewsPortlet = $$(byXpath("//a[contains(text(),'" + contentName + "')]/ancestor::section[1]/footer/strong"));

        if (allStatusesViewAsAnnotation.get(0).exists()) {
            allStatuses = allStatusesViewAsAnnotation;
        }
        else if (allStatusesViewAsTable.get(0).exists()) {
            allStatuses = allStatusesViewAsTable;
        }
        else {
            allStatuses = allStatusesViewAsAnnotationOfNewsPortlet;
        }

        if (allStatuses.size() < 1) {
            Assert.fail("Для контента " + contentName + " не получилось определить список статусов");
        }

        return allStatuses;
    }

    //Передаём нужные статусы из класса Constants
    @Step("Сравнение фактических статусов конента с ожидаемыми")
    public void checkStatuses(String contentName, String ... expectedStatuses) {
        ElementsCollection allStatuses = getAllStatusesByName(contentName);

        if (allStatuses.size() == expectedStatuses.length) {
            Collection<String> expectedStatusList = new ArrayList<>(Arrays.asList(expectedStatuses));
            for (String currentStatus : allStatuses.texts()) {
                //System.out.println("СТАТУС === " + currentStatus);
                if (!expectedStatusList.contains(currentStatus)) {
                    Assert.fail("Текущий статус контента " + contentName + "[" + currentStatus + "]" +
                            " не соответствует ожидаемым статусам " + expectedStatusList.toString());
                }
            }
        } else {
            Assert.fail("Количество фактических статусов (" + allStatuses.size() +
                    ") не совпадает с ожидаемыми (" + expectedStatuses.length + ") для контента " + contentName);
        }
    }

}
