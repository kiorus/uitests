package ru.emdev.uitests.core.portlets.publisher;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.testng.Assert;
import ru.emdev.uitests.core.Module;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class InKnowAssetPublisherFavorites extends Module {

    public InKnowAssetPublisherFavorites(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    @Step("Запрос ID айфрейма из Попапа")
    private String getIFrameIdFromPopup() {
        return $(byXpath("//iframe[contains(@id,'" + super.getInstanceId() + "_viewAsset_iframe_')]")).shouldBe(Condition.visible).getAttribute("id");
    }

    /*
    Проверка, что контент уже в "Избранном"
    */
    @Step("Проверка, что контент уже в \"Избранном\"")
    public boolean checkAddContentToFavoriteStatusById(String contentId, boolean contentOpenIntoPopup) {
        String iconStarXPath = "//a[contains(@href,'_assetEntryId=" + contentId + "') and contains(@class,'iap-fav-ctrl_state_on')]";
        sleep(1000);
        switchTo().defaultContent();
        $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]")).shouldBe(Condition.visible);
        if (contentOpenIntoPopup) {
            switchTo().frame(getIFrameIdFromPopup());
            return $(byXpath(iconStarXPath)).exists();
        } else {
            return $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]" + iconStarXPath)).exists();
        }
    }

    @Step("Добавление контента в Избранное используя ID")
    public void clickAddContentToTheFavoriteById(String contentId, boolean contentOpenIntoPopup) {
        String iconStarXPath = "//a[contains(@href,'_assetEntryId=" + contentId + "')]/span[contains(@class,'iap-fav-ctrl__icon-add')]";

        switchTo().defaultContent();
        if (contentOpenIntoPopup) {
            if (!checkAddContentToFavoriteStatusById(contentId, true)) {
                switchTo().frame(getIFrameIdFromPopup());
                $(byXpath(iconStarXPath)).shouldBe(Condition.visible).click();
                sleep(1000);
            } else {
                Assert.fail("Контент " + contentId + " уже находится в избранном");
            }
        } else {
            if (!checkAddContentToFavoriteStatusById(contentId, false)) {
                $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]" + iconStarXPath)).shouldBe(Condition.visible).click();
                sleep(1000);
            } else {
                Assert.fail("Контент " + contentId + " уже находится в избранном");
            }
        }
    }

    /*
    Удаление контента из "Избранного"
    */
    @Step("Удаление контента из избранного используя ID")
    public void clickRemoveContentFromTheFavoriteById(String contentId, boolean contentOpenIntoPopup) {
        String iconStarXPath = "//a[contains(@href,'_assetEntryId=" + contentId + "')]/span[contains(@class,'iap-fav-ctrl__icon-remove')]";

        switchTo().defaultContent();
        $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]")).shouldBe(Condition.visible);
        if (contentOpenIntoPopup) {
            if (checkAddContentToFavoriteStatusById(contentId, true)) {
                switchTo().frame(getIFrameIdFromPopup());
                $(byXpath(iconStarXPath)).shouldBe(Condition.visible).click();
                sleep(1000);
            } else {
                Assert.fail("Контент " + contentId + " не найден");
            }
        } else {
            if (checkAddContentToFavoriteStatusById(contentId, false)) {
                $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]" + iconStarXPath)).shouldBe(Condition.visible).click();
                sleep(1000);
            } else {
                Assert.fail("Контент " + contentId + " не найден");
            }
        }
    }

}
