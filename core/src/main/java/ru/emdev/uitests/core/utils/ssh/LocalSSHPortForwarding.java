package ru.emdev.uitests.core.utils.ssh;

import com.jcraft.jsch.JSchException;
import ru.emdev.uitests.core.ReadCustomProperties;

public class LocalSSHPortForwarding extends SSHSession {

    public LocalSSHPortForwarding(ReadCustomProperties sshPropertiesFile, boolean authByPrivateKey) {
        super(sshPropertiesFile, authByPrivateKey);
    }

    public void open() throws JSchException {
        super.createSession();
        super.getSSHSession().setPortForwardingR(super.getSSHLocalPort(), super.getSSHLocalHost(), super.getSSHRemotePort());
    }

    public void close() throws JSchException {
        super.closeSession();
    }
}
