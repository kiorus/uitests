package ru.emdev.uitests.core.utils.ssh;

import com.jcraft.jsch.*;
import ru.emdev.uitests.core.ReadCustomProperties;

public class SSHSession {

    JSch jSch = new JSch();

    private String sshHost;
    private int sshPort;
    private String sshUser;
    private String sshUserPassword;
    private String sshKeyAbsolutePath;
    private String sshKeyPassword;
    private String sshLocalHost;
    private int sshLocalPort;
    private int sshRemotePort;
    boolean authByPrivateKey;
    Session session;

    protected SSHSession(ReadCustomProperties sshPropertiesFile, boolean authByPrivateKey) {
        this.authByPrivateKey = authByPrivateKey;
        this.sshHost = sshPropertiesFile.readProperty("sshHost");
        this.sshPort = Integer.parseInt(sshPropertiesFile.readProperty("sshPort"));
        this.sshUser = sshPropertiesFile.readProperty("sshUser");
        this.sshUserPassword = sshPropertiesFile.readProperty("sshUserPassword");
        this.sshKeyAbsolutePath = sshPropertiesFile.readProperty("sshKeyAbsolutePath");
        this.sshKeyPassword = sshPropertiesFile.readProperty("sshKeyPassword");
        this.sshLocalHost = sshPropertiesFile.readProperty("sshLocalHost");
        this.sshLocalPort = Integer.parseInt(sshPropertiesFile.readProperty("sshLocalPort"));
        this.sshRemotePort = Integer.parseInt(sshPropertiesFile.readProperty("sshRemotePort"));
    }

    protected void createSession() throws JSchException {
            session = jSch.getSession(sshUser, sshHost, sshPort);
            // проверка подлиности ключа Хоста к которому подключаемся
            session.setConfig("StrictHostKeyChecking", "no");

            if (authByPrivateKey) {
                jSch.addIdentity(sshKeyAbsolutePath, sshKeyPassword);
            } else {
                session.setPassword(sshUserPassword);
            }

            session.connect(10 * 1000);
    }

    protected void closeSession() throws JSchException {
        if (authByPrivateKey) {
            jSch.removeAllIdentity();
        }
        session.disconnect();
    }

    protected Session getSSHSession() {
        return session;
    }

    protected String getSSHLocalHost() {
        return sshLocalHost;
    }

    protected int getSSHLocalPort() {
        return sshLocalPort;
    }

    protected int getSSHRemotePort() {
        return sshRemotePort;
    }
}
