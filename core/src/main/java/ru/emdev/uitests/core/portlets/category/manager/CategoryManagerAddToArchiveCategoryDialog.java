package ru.emdev.uitests.core.portlets.category.manager;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class CategoryManagerAddToArchiveCategoryDialog {

    private String confirmButtonXPath = "//clay-dialog[@trigger='acmCtrl.showArchCategoryDialog']//clay-dialog-primary-button/../../button";

    protected void addToArchiveWithReplaceContent(String vocabularyName, String categoryNameForArchiving, String newCategoryNameForContent) {
        //Нажать кнопку "Заархивировать категорию" в портлете "Управление категориями"
        $(byXpath("//span[contains(text(),'" + vocabularyName + "')]/../../..//span[text()='" + categoryNameForArchiving +
                "']/..//span[@class='icon-download-alt']")).shouldBe(Condition.visible).click();
        //В диалоге выбрать пункт 2 - архивация категории с перемещением контета в другую категорию
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showArchCategoryDialog']//input[@ng-model='acmCtrl.categoryDialog.moveObjects' and @ng-value='true']/../span"))
                .shouldBe(Condition.visible).click();
        //Нажать кнопку "Выбрать", для выбора категории
        $(byXpath("//clay-dialog[@trigger='acmCtrl.showArchCategoryDialog']//button[@ng-click='openDialog()']")).shouldBe(Condition.visible).click();
        //В появившемся диалоге ввести название искомой категории
        $(byXpath("//clay-dialog[@trigger='showDialog'][2]//input[@ng-model='searchStrContainer.searchStr']")).shouldBe(Condition.visible).setValue(newCategoryNameForContent);
        //Выбрать найденную категорию из списка
        $(byXpath("//tr[@ng-repeat='category in categoriesList']//span[text()='" + newCategoryNameForContent + "']")).shouldBe(Condition.visible).click();
        //Нажать кнопку "Продолжить"
        $(byXpath("//clay-dialog[@trigger='showDialog'][2]//clay-dialog-primary-button/../../button")).shouldBe(Condition.visible).click();
        //Нажать кнопку "Подтвердить", чтобы переместить контент в другую категорию
        $(byXpath(confirmButtonXPath)).shouldBe(Condition.visible).click();
    }

    protected void addToArchiveWithContent(String vocabularyName, String categoryNameForArchiving) {
        $(byXpath("//span[contains(text(),'" + vocabularyName + "')]/../../..//span[text()='" + categoryNameForArchiving +
                "']/..//span[@class='icon-download-alt']")).shouldBe(Condition.visible).click();
        $(byXpath(confirmButtonXPath)).shouldBe(Condition.visible).click();
    }
}
