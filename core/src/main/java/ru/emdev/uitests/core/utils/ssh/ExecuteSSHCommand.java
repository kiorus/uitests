package ru.emdev.uitests.core.utils.ssh;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import ru.emdev.uitests.core.ReadCustomProperties;

import static java.lang.Thread.sleep;

public class ExecuteSSHCommand extends SSHSession {

    //FakeSMTP должен быть в домашней директории пользователя rolf
    //Должна быть в домашней директории пользователя rolf папка для писем fakesmtp_mails
    //private static String commandToStartFakeSMTP = "java -jar fakeSMTP-1.13.jar -b -s -o fakesmtp_mails -p 25555";
    //private static String commandToStopFakeSMTP = "ps aux | grep fakesmtp | grep -v grep | awk {'print $2'} | xargs kill -9";

    public ExecuteSSHCommand(ReadCustomProperties sshPropertiesFile, boolean authByPrivateKey, String command) throws JSchException, InterruptedException {
        super(sshPropertiesFile, authByPrivateKey);
        super.createSession();

        // отправка команд на сервер
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);
        channel.setInputStream(null);
        channel.setOutputStream(System.out);
        channel.connect(10 * 1000);
        sleep(5 * 1000); // чтобы команда на сервере успела отработать

        channel.disconnect();
        super.closeSession();
    }
}
