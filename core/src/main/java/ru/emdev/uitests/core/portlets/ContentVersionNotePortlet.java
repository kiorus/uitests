package ru.emdev.uitests.core.portlets;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class ContentVersionNotePortlet {

    private void typeNote(String note) {
        $(byXpath("//textarea[@id='_ru_emdev_inknowledge_content_version_note_web_portlet_ContentVersionNotePortlet_contentVersionNote']"))
                .shouldBe(Condition.visible).setValue(note);
        sleep(500);
    }

    @Step("Создаётся/изменяется контент с написанием заметки")
    public void typeAndSaveNote(String note) {
        sleep(1000);
        switchTo().frame(0);
        typeNote(note);
        $(byXpath("//button[@id='save']/span")).shouldBe(Condition.visible).click();
    }

    @Step("Создаётся/изменяется контент без заметки")
    public void skipNote() {
        sleep(1000);
        switchTo().frame(0);
        $(byXpath("//button[@id='skip']/span")).shouldBe(Condition.visible).click();
    }

}
