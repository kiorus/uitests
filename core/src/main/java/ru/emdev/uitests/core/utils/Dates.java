package ru.emdev.uitests.core.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Утилитный класс для получения дат и времени в строковом представлении
 */
public class Dates {

    private static final String DATE_FORMAT = "dd.MM.yyyy";
    private static final String TIME_FORMAT = "hh:mm:ss";
    private static final String DATE_TIME_FORMAT = DATE_FORMAT + " " + TIME_FORMAT;

    private Dates() {
    }

    /**
     * Возвращает текущую дату и время в строковом представлении
     *
     * @return Дата и время в формате dd.MM.yyyy hh:mm:ss
     */
    public static String getCurrentDateTime() {
        return new SimpleDateFormat(DATE_TIME_FORMAT).format(new Date());
    }

    /**
     * Прибавляет к текущей дате указанное количество дней и возвращает в строковом формате
     *
     * @param days количество дней, которые необходимо прибавить к текущей дате
     * @return Дата в формате dd.MM.yyyy
     */
    public static String getCurrentDatePlusDays(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return new SimpleDateFormat(DATE_FORMAT).format(calendar.getTime());
    }
}
