package ru.emdev.uitests.core.ckeditor;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class CKEditorContent {

    String predictiveContentsListXPath = "//ul[contains(@class,'cke_autocomplete_panel')]/li";

    protected CKEditorContent() {
    }

    protected void typeContent(String content) {

        switchTo().defaultContent();
        switchTo().frame(0);
        $(byXpath("//body[contains(@class,'html-editor')]")).shouldBe(Condition.visible).setValue(content);
    }

    protected ElementsCollection getPredictiveContentsList() {
        switchTo().defaultContent();
        return $$(byXpath(predictiveContentsListXPath));
    }

    protected void setPredictiveContentFromList(int contentIndex) {
        switchTo().defaultContent();
        $(byXpath(predictiveContentsListXPath + "[" + contentIndex + "]")).shouldBe(Condition.exist).click();
    }

    protected void setPredictiveContentFromList(String contentName) {
        switchTo().defaultContent();
        $(byXpath(predictiveContentsListXPath + "[contains(text(),'" + contentName + "')]")).click();
    }

    protected boolean checkPredictiveContentLink(String contentId, String contentName) {
        switchTo().defaultContent();
        switchTo().frame(0);
        return $(byXpath("//body/p/a[contains(@href,'" + contentId + "') and text()='" + contentName + "']")).exists();
    }

}
