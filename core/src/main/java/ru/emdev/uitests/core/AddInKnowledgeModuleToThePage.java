package ru.emdev.uitests.core;

public class AddInKnowledgeModuleToThePage extends AddModuleToThePage {

    @Deprecated
    public AddInKnowledgeModuleToThePage(String subMenuItemName) {
        super(Constants.ROOT_MENU_WIDGETS, Constants.PARENT_MENU_INKNOWLEDGE, subMenuItemName);
    }

    public AddInKnowledgeModuleToThePage(String subMenuItemName, String columnId) {
        super(Constants.ROOT_MENU_WIDGETS, Constants.PARENT_MENU_INKNOWLEDGE, subMenuItemName, columnId);
    }
}
