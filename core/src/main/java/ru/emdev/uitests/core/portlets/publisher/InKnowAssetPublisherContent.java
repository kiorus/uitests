package ru.emdev.uitests.core.portlets.publisher;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.testng.Assert;
import ru.emdev.uitests.core.Module;
import ru.emdev.uitests.core.ReadCustomProperties;
import ru.emdev.uitests.core.utils.JavaScriptActions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class InKnowAssetPublisherContent extends Module {

    private ReadCustomProperties portalAuthProperties =
            new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    private final String testPage = portalAuthProperties.readProperty("testPage");

    public InKnowAssetPublisherContent(String moduleTitleOnThePage) {
        super(moduleTitleOnThePage);
    }

    @Step("Добываем все id-шники видимого контента публикатора")
    private List<String> getContentsIdsCollection(ElementsCollection collectionXPath) {
        List<String> contentIds = new ArrayList<>();
        if (collectionXPath.size()>0) {
            for (SelenideElement content : collectionXPath) {
                String patternString = "(assetEntryId%\\d+D)(\\d+)";
                Pattern pattern = Pattern.compile(patternString);
                Matcher matcher = pattern.matcher(content.getAttribute("href"));
                if (matcher.find()) {
                    contentIds.add(matcher.group().replaceAll(patternString, "$2"));
                } else {
                    Assert.fail("Не получилось извлечь id контента с помощью регулярного выражения");
                }
            }
        } else {
            Assert.fail("Список id-шников пуст");
        }
        return contentIds;
    }

    @Step("Запрос ID контента")
    public List<String> getContentIds() {
        /*
        TODO Нужно добавить в этот метод работу с пагинацией. Но так как у нас пока контент не удаляется (не перемещается в корзину),
         есть риск что через некоторое время придется обрабатываать слишком много не нужного контента. Сначала надо прикрутить удаление контента
        */
        SelenideElement contentTable = $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//table"));
        ElementsCollection contentsViewAsTable = $$(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//td[1]/a"));
        ElementsCollection contentsViewAsAnnotation = $$(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]" +
                "//*[@class='component-title']/a[contains(@class,'asset-title')]"));
        ElementsCollection contentsViewAsAnnotationOfNewsPortlet = $$(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//section//h2/a"));

        List<String> contentIds;
        if (contentTable.exists()) {
            contentIds = getContentsIdsCollection(contentsViewAsTable);
        }
        else if (contentsViewAsAnnotation.get(0).exists()) {
            contentIds = getContentsIdsCollection(contentsViewAsAnnotation);
        }
        else {
            contentIds = getContentsIdsCollection(contentsViewAsAnnotationOfNewsPortlet);
        }

        return contentIds;
    }

    @Step("Запрос на получение ID контента по его имени")
    public String getContentIdByName(String contentName) {
        SelenideElement content = $(byXpath("//a[contains(@href,'" + super.getInstanceId() + "') and contains(text(),'" + contentName + "')]"));
        String contentId = null;

        String patternString = "(assetEntryId%\\d+D)(\\d+)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(content.getAttribute("href"));
        if (matcher.find()) {
            contentId = matcher.group().replaceAll(patternString, "$2");
        } else {
            Assert.fail("Не получилось извлечь id контента с помощью регулярного выражения");
        }

        return contentId;
    }

    @Step("Получение имени контента по ID")
    public String getContentNameById(String contentId) {
        return $(byXpath("//a[contains(@href,'" + super.getInstanceId() + "_assetEntryId%3D" + contentId + "')]/..")).text().trim();
    }

/*
    TODO Начиная с версии Liferay 7.2.0 GA1 весь контент открывается на создание и редактирование через элементы "админки".
        Нужно выкосить из этого класса не нужные методы или вызывать из них методы из класса ControlPanelJournalPortletEditContent
*/
    /*
    Редактирование контента
     */
    @Step("Нажатие на кнопку сохранение как черновика")
    public void clickSaveAsDraftButton() {
        $(byXpath("//button[@id='_com_liferay_journal_web_portlet_JournalPortlet_saveButton']")).shouldBe(Condition.visible).click();
    }

    @Step("Нажатие на кнопку публикации")
    public void clickPublishButton() {
        $(byXpath("//button[@id='_com_liferay_journal_web_portlet_JournalPortlet_publishButton']")).shouldBe(Condition.visible).click();
    }

    @Step("Нажатие на кнопку отмена")
    public void clickCancelButton() {
        $(byXpath("//a[contains(@class,'btn-cancel')]/span")).shouldBe(Condition.visible).click();
    }

    @Step("Изменение контента")
    public void editContentById(String contentId) {
        JavaScriptActions.hoverThroughJavascript(super.getModuleOnThePage());
        //Нажать на три точки справа от имени контента для вызова меню определённого контента
        $(byXpath("//span[contains(@id,'" + contentId + "')]/..//a[contains(@class,'dropdown-toggle') and " +
                "contains(@id,'_ru_emdev_inknowledge_asset_publisher_web_portlet_AssetPublisherPortlet_" +
                super.getInstanceId() + "')]/span")).shouldBe(Condition.visible).click();
        //Выбрать пункт меню "Изменить"
        $(byXpath("//div[@class='open lfr-icon-menu-open']/ul/li[1]/a/span")).shouldBe(Condition.visible).click();
        sleep(2000);
    }

    /*
    Привязывание контента к категории
     */
    @Step("Проверка категорий")
    private boolean categoryIsChecked(String categoryName) {
        SelenideElement element = $(byXpath("//div[@data-treeitemname='" + categoryName + "']"));
        element.shouldBe(Condition.visible);
        return executeJavaScript("return arguments[0].classList.contains('selected')", element);
    }

    @Step("Привязывание контента к категории")
    public void assignContentToCategory(String vocabularyName, String categoryName) {
        SelenideElement metaDataSection = $(byXpath("//a[@aria-controls='metadataContent']"));
        SelenideElement choiceVocabulary = $(byXpath("//label[text()='" + vocabularyName + "']/..//button[@class='btn btn-secondary']"));

        metaDataSection.shouldBe(Condition.visible);
        boolean metaDataSectionCollapsed = executeJavaScript("return arguments[0].classList.contains('collapsed')", metaDataSection);
        if (metaDataSectionCollapsed) {
            metaDataSection.shouldBe(Condition.visible).click();
        }
        choiceVocabulary.shouldBe(Condition.visible).click();
        sleep(2000);
        switchTo().defaultContent();
//        iframe для поиска и выбора категорий
        switchTo().frame("_com_liferay_asset_categories_selector_web_portlet_AssetCategoriesSelectorPortlet_selectCategory_iframe_");
        $(byXpath("//input[@name='_com_liferay_asset_categories_selector_web_portlet_AssetCategoriesSelectorPortlet_filterKeywords']"))
                .shouldBe(Condition.visible).setValue(categoryName);
        sleep(1000);
        if (!categoryIsChecked(categoryName)) {
            $(byXpath("//span[text()='" + categoryName + "']")).shouldBe(Condition.visible).click();
            sleep(1000);
        }
        switchTo().defaultContent();
        $(byXpath("//button[@id='addButton']")).shouldBe(Condition.visible).click();
        sleep(2000);
    }

    /*
    Проверка контента
     */
    @Step("Проверка назначеных контенту категорий")
    public boolean checkAssignContentToCategories(String contentId, String ... categoriesNames) {
        boolean result = false;
        List<String> categoriesIdList = new ArrayList<>();
        switchTo().defaultContent();
        sleep(2000);

        String contentXPath = "//a[contains(text(),'" + getContentNameById(contentId) + "')]/../../..";
        String categoryXPath = contentXPath + "//span[@class='taglib-asset-categories-summary']/a";
        $(byXpath(contentXPath)).shouldBe(Condition.visible);
        ElementsCollection categoriesList = $$(byXpath(categoryXPath));

        int categoriesCount = categoriesList.size();
        if (categoriesCount>0) {
            for (int counter = 1; counter <= categoriesCount; counter++) {
                String categoryId = categoriesList.listIterator(counter-1).next().getAttribute("href");
                Pattern pattern = Pattern.compile("p_r_p_categoryId=\\d*");
                Matcher matcher = pattern.matcher(categoryId);
                if (matcher.find()) {
                    categoriesIdList.add(matcher.group());
                } else {
                    Assert.fail("Что-то пошло не так ... У категории отсутствует id ?");
                }
//                System.out.println(categoriesIdList.get(counter-1) + " " + categoriesList.get(counter-1).getText());
            }
            for (String categoryName : categoriesNames) {
                for (int counter = 1; counter <= categoriesCount; counter++) {
                    String contentCategoryText = $(byXpath(categoryXPath + "[contains(@href,'" + testPage +
                            "?p_p_id=ru_emdev_inknowledge_asset_publisher_web_portlet_AssetPublisherPortlet_" + super.getInstanceId() +
                            "&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&" + categoriesIdList.get(counter-1) + "')]")).getText();
                    if (contentCategoryText.contains(categoryName)) {
                        result = true;
                        break;
                    } else {
                        result = false;
                    }
                }
                if (!result) {
                    break;
                }
            }
        }
        return result;
    }

    @Step("Открытие контента")
    public void openContent(String contentId) {
        $(byXpath("//section[contains(@id,'" + super.getInstanceId() + "')]//a[contains(@href,'_assetEntryId%3D" + contentId + "') or " +
                "contains(@href,'_assetEntryId%253D" + contentId + "')]")).shouldBe(Condition.visible).click();
    }
}
