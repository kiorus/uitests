package ru.emdev.uitests.core.auth;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import ru.emdev.uitests.core.ReadCustomProperties;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class PortalAuthPage {

    private ReadCustomProperties portalAuthProperties;

    public PortalAuthPage() {
        portalAuthProperties = new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    }

    private SelenideElement loginInput = $(byXpath("//input[@id='_com_liferay_login_web_portlet_LoginPortlet_login']"));
    private SelenideElement passwordInput = $(byXpath("//input[@id='_com_liferay_login_web_portlet_LoginPortlet_password']"));
    private SelenideElement loginSubmitButton = $(byXpath("//button[contains(@id,'_com_liferay_login_web_portlet_LoginPortlet')]"));

    @Step("Авторизация на портале используя указанные данные")
    public void auth() {
        open("/c/portal/login");
        if (loginInput.exists()) {
            Allure.addAttachment("Учетные данные", portalAuthProperties.readProperty("adminUser") + " "
                    + portalAuthProperties.readProperty("adminUserPassword"));
            loginInput.setValue(portalAuthProperties.readProperty("adminUser"));
            passwordInput.setValue(portalAuthProperties.readProperty("adminUserPassword"));
            loginSubmitButton.click();
            sleep(3000);
        }
    }

}
