package ru.emdev.uitests.core.controlpanel;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.testng.Assert;
import ru.emdev.uitests.core.ckeditor.CKEditorContent;


import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ControlPanelJournalPortletEditContent extends CKEditorContent {

    private String contentName;

    @Step("Вставка нового имени контента")
    public void typeContentName(String contentName) {
        this.contentName = contentName;
        sleep(1000);
        $(byXpath("//input[@id='_com_liferay_journal_web_portlet_JournalPortlet_titleMapAsXML']"))
                .shouldBe(Condition.visible).setValue(contentName);
    }

    @Step("Вставка тела контента")
    public void typeContentBodyIntoCKEditor(String content) {
        sleep(10000);
        super.typeContent(content);
    }

    public ElementsCollection getPredictiveContentsList() {
        Assert.assertTrue(super.getPredictiveContentsList().size() > 0, "Не найден ни один результат в предиктивном поиске");

        return super.getPredictiveContentsList();
    }

    public void setPredictiveContentFromList(int contentIndex) {
        super.setPredictiveContentFromList(contentIndex);
    }

    public void setPredictiveContentFromList(String contentName) {
        super.setPredictiveContentFromList(contentName);
    }

    public boolean checkPredictiveContentLink(String contentId, String contentName) {
        return super.checkPredictiveContentLink(contentId, contentName);
    }

    @Step("Открытие расширенного списка контента")
    private void openExpandedScheduleContent() {
        SelenideElement element = $(byXpath("//a[@aria-controls='scheduleContent' and @aria-expanded='false']/span"));
        if (element.exists()) {
            element.click();
        }
    }

    //Формат даты в данном случае dd.MM.yyyy
    @Step("Ввод нового значения даты в формате dd.MM.yyyy")
    public void typeNewDisplayDate(String newDate) {
        openExpandedScheduleContent();
        $(byXpath("//input[@id='_com_liferay_journal_web_portlet_JournalPortlet_displayDate']"))
                .shouldBe(Condition.visible).setValue(newDate);
    }

     @Step("Клик на чекбос \"Никогда не истекает\"")
    private void clickNeverExpireCheckbox() {
        openExpandedScheduleContent();
        SelenideElement element = $(byXpath("//input[@name='_com_liferay_journal_web_portlet_JournalPortlet_neverExpire']"));
        if (!element.isSelected()) {
            element.setSelected(true);
        }
    }

    @Step("Закрытие сообщения подтверждения")
    private void clickCloseSubmitMessage() {
        $(byXpath("//div[@id='alertContainer']//button")).shouldBe(Condition.visible).click();
    }

    @Step("Нажатие на кнопку публикации")
    public void clickPublishButton() {
        clickNeverExpireCheckbox();
        $(byXpath("//button[@id='_com_liferay_journal_web_portlet_JournalPortlet_publishButton']")).shouldBe(Condition.visible).click();
//        clickCloseSubmitMessage();
    }

    @Step("Нажатие на кнопку \"Сохранить как черновик\"")
    public void clickSaveAsDraftButton() {
        clickNeverExpireCheckbox();
        $(byXpath("//button[@id='_com_liferay_journal_web_portlet_JournalPortlet_saveButton']")).shouldBe(Condition.visible).click();
//        clickCloseSubmitMessage();
    }

    @Step("Нажатие на кнопку Отмена")
    public void clickCancelButton() {
        $(byXpath("//a[contains(@class,'btn-cancel')]/span")).shouldBe(Condition.visible).click();
    }

    @Attachment
    public String getContentName() {
        return this.contentName;
    }

}
