package ru.emdev.uitests.core;

public class Constants {

    public static final String ROOT_MENU_WIDGETS = "Портлеты";
    public static final String PARENT_MENU_INKNOWLEDGE = "InKnowledge";
    public static final String SUBMENU_ITEM_FAVORITES = "Избранное";
    public static final String SUBMENU_ITEM_ASSET_PUBLISHER = "Публикатор (InKnowledge)";
    public static final String SUBMENU_ITEM_CATEGORY_MANAGER = "Управление категориями";
    public static final String SUBMENU_ITEM_NEWS = "Новости";
    public static final String SUBMENU_ITEM_ARTICLE = "Список контента";

    public static final String WORKFLOW_STATUS_APPROVED = "УТВЕРЖДЕНО";
    public static final String WORKFLOW_STATUS_DRAFT = "ЧЕРНОВИК";
    public static final String WORKFLOW_STATUS_EXPIRED = "УСТАРЕВШАЯ";
    public static final String WORKFLOW_STATUS_SCHEDULED = "ЗАПЛАНИРОВАНО";

    public static final String CATEGORY_MANAGER_CONFIG_OPTION_SCOPE_SITE = "site";
    public static final String CATEGORY_MANAGER_CONFIG_OPTION_SCOPE_ALL = "all";
    public static final String CATEGORY_MANAGER_CONFIG_OPTION_SCOPE_GLOBAL = "global";
    public static final String CATEGORY_MANAGER_CONFIG_OPTION_ALL_DICTIONARIES = "true";
    public static final String CATEGORY_MANAGER_CONFIG_OPTION_CHOICE_DICTIONARIES = "false";
    public static final String CATEGORY_MANAGER_CONFIG_OPTION_DISPLAY_SIMPLE = "simple";
    public static final String CATEGORY_MANAGER_CONFIG_OPTION_DISPLAY_DRAG_AND_DROP = "dragAndDrop";

    public static final String ASSET_PUBLISHER_CONFIG_METADATA_CATEGORIES_FIELD = "categories";

    public static final String LAYOUT_COLUMN_1 = "//div[@id='layout-column_column-1']";
    public static final String LAYOUT_COLUMN_2 = "//div[@id='layout-column_column-2']";

    public static final String PUBLISHER_BUTTON_TYPE_TOP = "1";
    public static final String PUBLISHER_BUTTON_TYPE_PREVIOUS = "2";
    public static final String PUBLISHER_BUTTON_TYPE_ONWARD = "3";
    public static final String PUBLISHER_BUTTON_TYPE_LAST = "4";

    public static final String OTHER_BUTTON_TYPE_PREVIOUS = "1";
    public static final String OTHER_BUTTON_TYPE_ONWARD = "2";

    public static final String PAGINATOR_OPTION_NONE = "1";
    public static final String PAGINATOR_OPTION_REGULAR = "2";
    public static final String PAGINATOR_OPTION_SIMPLE = "3";

}
