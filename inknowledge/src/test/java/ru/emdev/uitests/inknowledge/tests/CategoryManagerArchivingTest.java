package ru.emdev.uitests.inknowledge.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.*;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletEditContent;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletPage;
import ru.emdev.uitests.core.controlpanel.ControlPanelRightMenu;
import ru.emdev.uitests.core.portlets.category.manager.CategoryManagerActions;
import ru.emdev.uitests.core.portlets.category.manager.CategoryManagerConfig;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherConfig;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherContent;
import ru.emdev.uitests.core.utils.Dates;
import ru.emdev.uitests.core.utils.JavaScriptActions;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class CategoryManagerArchivingTest {

    private CategoryManagerConfig categoryManagerConfig = new CategoryManagerConfig(Constants.SUBMENU_ITEM_CATEGORY_MANAGER);
    private InKnowAssetPublisherConfig inKnowAssetPublisherConfig =
            new InKnowAssetPublisherConfig(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
    private InKnowAssetPublisherContent inKnowAssetPublisherContent =
            new InKnowAssetPublisherContent(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
    private ControlPanelRightMenu controlPanelRightMenu =
            new ControlPanelRightMenu(Constants.ROOT_MENU_WIDGETS, Constants.PARENT_MENU_INKNOWLEDGE);
    private ControlPanelJournalPortletPage controlPanelJournalPortletPage =
            new ControlPanelJournalPortletPage();
    private ControlPanelJournalPortletEditContent controlPanelJournalPortletEditContent =
            new ControlPanelJournalPortletEditContent();
    private CheckModule checkAssetPublisherModule = new CheckModule("ru.emdev.inknowledge.asset.publisher.web");
    private CheckModule checkCategoryManagerModule = new CheckModule("ru.emdev.inknowledge.categorymanager.web");
    private ReadCustomProperties projectProperties =
            new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    private final String testPage = projectProperties.readProperty("testPage");
    private String usedContentId;
    private String vocabularyName;
    private String categoryName1, categoryName2;
    private String subCategoryName1, subCategoryName2;
    private CategoryManagerActions categoryManagerActions;
    private List<String> contentNames;
    private boolean isAssign;

    private String generateVocabularyOrCategoryName(boolean isCategory) {
        String name;
        if (isCategory) {
            name = "Category ";
        } else {
            name = "Vocabulary ";
        }
        return name + Dates.getCurrentDateTime();
    }

    private String generateContentName() {
        return "AutoTest " + Dates.getCurrentDateTime();
    }

    @Test(description = "Убедиться, что модули 'Публикатор' и 'Менеджер категорий' активны и присутствуют в \"+\"",
            retryAnalyzer = RunTestAgain.class)
    public void checkAvailabilityOfAssetPublisherModuleInTheControlMenu() {
        checkAssetPublisherModule.checkModuleStatus();
        checkCategoryManagerModule.checkModuleStatus();
        if (checkAssetPublisherModule.getModuleStatus() && checkCategoryManagerModule.getModuleStatus()) {
            open(testPage);
            controlPanelRightMenu.clickPlusButton();
            controlPanelRightMenu.clickRootMenu();
            controlPanelRightMenu.clickParentMenu();
        }
        Assert.assertTrue(controlPanelRightMenu.checkSubMenuItem(Constants.SUBMENU_ITEM_ASSET_PUBLISHER),
                "Модуль '" + Constants.SUBMENU_ITEM_ASSET_PUBLISHER + "' не установлен или сломан");
        Assert.assertTrue(controlPanelRightMenu.checkSubMenuItem(Constants.SUBMENU_ITEM_CATEGORY_MANAGER),
                "Модуль '" + Constants.SUBMENU_ITEM_CATEGORY_MANAGER + "' не установлен или сломан");
    }

    @Test(description = "Добавление на тестовую страницу необходимых портлетов",
            dependsOnMethods = "checkAvailabilityOfAssetPublisherModuleInTheControlMenu")
    public void addTheRequiredModules() {
        open(testPage);
        controlPanelRightMenu.clickPlusButton();
        controlPanelRightMenu.clickRootMenu();
        controlPanelRightMenu.clickParentMenu();
        new AddInKnowledgeModuleToThePage(Constants.SUBMENU_ITEM_ASSET_PUBLISHER, Constants.LAYOUT_COLUMN_2);
        new AddInKnowledgeModuleToThePage(Constants.SUBMENU_ITEM_CATEGORY_MANAGER, Constants.LAYOUT_COLUMN_1);
    }

    @Test(description = "Настройка портлета Менеджер Категорий",
            dependsOnMethods = "addTheRequiredModules",
            retryAnalyzer = RunTestAgain.class)
    public void prepareAssetPublisherPortlet() {
        open(testPage);
        inKnowAssetPublisherConfig.openConfiguration();
        inKnowAssetPublisherConfig.addMetadataContent(Constants.ASSET_PUBLISHER_CONFIG_METADATA_CATEGORIES_FIELD);
        inKnowAssetPublisherConfig.clickSubmitButtonAndCloseConfiguration();
    }

    @Test(description = "Настройка портлета Менеджер Категорий",
            dependsOnMethods = "addTheRequiredModules",
            retryAnalyzer = RunTestAgain.class)
    public void prepareCategoryManagerPortlet() {
        open(testPage);
        categoryManagerConfig.openConfiguration();
        categoryManagerConfig.changeScope(Constants.CATEGORY_MANAGER_CONFIG_OPTION_SCOPE_SITE);
        categoryManagerConfig.changeVocabularies(Constants.CATEGORY_MANAGER_CONFIG_OPTION_ALL_DICTIONARIES);
        categoryManagerConfig.changeDisplay(Constants.CATEGORY_MANAGER_CONFIG_OPTION_DISPLAY_SIMPLE);
        categoryManagerConfig.enableEditMode();
        categoryManagerConfig.enableAppointResponsible();
        categoryManagerConfig.clickSaveButton();
    }

    @Test(description = "Создание словарей",
            dependsOnMethods = {"prepareCategoryManagerPortlet","prepareAssetPublisherPortlet"},
            retryAnalyzer = RunTestAgain.class)
    public void createVocabularies() {
        open(testPage);
        vocabularyName = generateVocabularyOrCategoryName(false);
        categoryManagerActions = new CategoryManagerActions(Constants.SUBMENU_ITEM_CATEGORY_MANAGER, vocabularyName);
        categoryManagerActions.addVocabulary(vocabularyName, false);
    }

    @Test(description = "Создание категорий",
            dependsOnMethods = "createVocabularies",
            retryAnalyzer = RunTestAgain.class)
    public void createCategoriesAndSubcategories() {
        open(testPage);
        categoryName1 = generateVocabularyOrCategoryName(true);
        sleep(1000);
        categoryName2 = generateVocabularyOrCategoryName(true);
        sleep(1000);
        subCategoryName1 = generateVocabularyOrCategoryName(true);
        sleep(1000);
        subCategoryName2 = generateVocabularyOrCategoryName(true);


        categoryManagerActions.addCategory(categoryName1, categoryName1, "");
        categoryManagerActions.addCategory(categoryName1, subCategoryName1, subCategoryName1, "");
        categoryManagerActions.addCategory(categoryName1, subCategoryName2, subCategoryName1, "");
        categoryManagerActions.addCategory(categoryName2, categoryName2, "");
    }

    @Test(description = "Подготовка контента для работы с категориями (создание, если требуется)",
            dependsOnMethods = "createCategoriesAndSubcategories",
            retryAnalyzer = RunTestAgain.class)
    public void createContent() {
        open(testPage);
        contentNames = new ArrayList<>();
        JavaScriptActions.disableAllTooltips();

        for ( int i=0; i<=1; i++) {
            controlPanelJournalPortletPage.openPage(testPage);
            controlPanelJournalPortletPage.addInknowledgeAtricle();
            String contentName = generateContentName();
            contentNames.add(contentName);
            controlPanelJournalPortletEditContent.typeContentName(contentName);
            controlPanelJournalPortletEditContent.clickPublishButton();
        }
    }

    @Test(description = "Привязывание контента к категориям",
            dependsOnMethods = "createContent",
            retryAnalyzer = RunTestAgain.class)
    public void assignContentToCategories() {
        open(testPage);
        JavaScriptActions.disableAllTooltips();
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(0));
        inKnowAssetPublisherContent.editContentById(usedContentId);
        inKnowAssetPublisherContent.assignContentToCategory(vocabularyName, subCategoryName1);
        inKnowAssetPublisherContent.clickPublishButton();
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, subCategoryName1);
        if (!isAssign) {
            Assert.fail("Контент " + contentNames.get(0) + " не является участником категорий " + subCategoryName1);
        }

        JavaScriptActions.disableAllTooltips();
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(1));
        inKnowAssetPublisherContent.editContentById(usedContentId);
        inKnowAssetPublisherContent.assignContentToCategory(vocabularyName, subCategoryName2);
        inKnowAssetPublisherContent.clickPublishButton();
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, subCategoryName2);
        if (!isAssign) {
            Assert.fail("Контент " + contentNames.get(1) + " не является участником категории " + subCategoryName2);
        }
    }

    @Test(description = "Архивирование подкатегории с перемещением контента в другую категорию",
            dependsOnMethods = "assignContentToCategories")
    public void archiveSubCategoryAndReplaceContent() {
        open(testPage);
        categoryManagerActions.archiveCategoryWithReplaceContent(subCategoryName1,categoryName2);

        //Проверям, что после архивации категории с переносом контента - контент остался в публикаторе
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(0));
        Assert.assertNotEquals(usedContentId, "",
                "Контент " + contentNames.get(0) + " должен был остаться в публикатое после архивации категории " + subCategoryName1);

        //Проверяем, что контент привязан к новой категории
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, categoryName2);
        Assert.assertNotEquals(isAssign, true,
                "Контент " + contentNames.get(0) + " не является участником категории " + categoryName2);
    }

    @Test(description = "Архивирование подкатегории с контентом",
            dependsOnMethods = "assignContentToCategories",
            retryAnalyzer = RunTestAgain.class)
    public void archiveSubCategoryWithContent() {
        open(testPage);
        categoryManagerActions.archiveCategoryWithContent(subCategoryName2);

        //Проверяем, что после архивации категории - контент не остался в публикаторе
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(1));
        Assert.assertNotEquals(usedContentId, "",
                "Контент " + contentNames.get(1) + " не должен был остаться в публикаторе после архивации категории " + subCategoryName2);
    }

    @Test(description = "Разархивирование подкатегории с контентом",
            dependsOnMethods = "assignContentToCategories",
            priority = 1)
    public void unArchiveSubCategoryWithContent() {
        open(testPage);
        categoryManagerActions.unArchiveCategory(subCategoryName2);
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(0));
        Assert.assertNotEquals(usedContentId, "",
                "Контент " + contentNames.get(0) + " должен был появиться в публикаторе после разархивации категории " + subCategoryName2);
    }

    @Test(description = "Удаление словаря со всеми категориями",
            dependsOnMethods = {"archiveSubCategoryWithContent","unArchiveSubCategoryWithContent"},
            alwaysRun = true,
            retryAnalyzer = RunTestAgain.class)
    public void removeVocabularyWithArchivedCategory() {
        open(testPage);
        archiveSubCategoryWithContent();
        categoryManagerActions.removeVocabularyWithAllCategories();
    }

    @AfterClass(description = "Удаление модулей со стартовой страницы")
    public void removeRequiredModules() {
        open(testPage);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_CATEGORY_MANAGER);
    }
}
