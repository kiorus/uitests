package ru.emdev.uitests.inknowledge.tests;

import com.codeborne.selenide.testng.ScreenShooter;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.*;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletEditContent;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletPage;
import ru.emdev.uitests.core.controlpanel.ControlPanelRightMenu;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherConfig;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherDraftAndArchive;
import ru.emdev.uitests.core.utils.Dates;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;

@Listeners({ ScreenShooter.class})
public class DraftsAndArchivesOfNewsTest {

    private CheckModule checkModule = new CheckModule("asset-publisher-favorites-module");
    private ReadCustomProperties projectProperties =
            new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    private final String testPage = projectProperties.readProperty("testPage");
    private ControlPanelRightMenu controlPanelRightMenu =
            new ControlPanelRightMenu(Constants.ROOT_MENU_WIDGETS, Constants.PARENT_MENU_INKNOWLEDGE);
    private InKnowAssetPublisherConfig inKnowAssetPublisherConfig =
            new InKnowAssetPublisherConfig(Constants.SUBMENU_ITEM_NEWS);
    private InKnowAssetPublisherDraftAndArchive inKnowAssetPublisherDraftAndArchive =
            new InKnowAssetPublisherDraftAndArchive(Constants.SUBMENU_ITEM_NEWS);
    private ControlPanelJournalPortletPage controlPanelJournalPortletPage =
            new ControlPanelJournalPortletPage();
    private ControlPanelJournalPortletEditContent controlPanelJournalPortletEditContent =
            new ControlPanelJournalPortletEditContent();
    private String contentNameWithDraftOnly, contentNameForArchiveOnly;
    private String contentNameWithAllStatuses, contentNameWithScheduleDateOnly;

    private String generateContentName() {
        return "AutoTest " + Dates.getCurrentDateTime();
    }

    @Test(description = "Убедиться, что модуль активен и присутствует в \"+\"",
            retryAnalyzer = RunTestAgain.class)
    public void checkAvailabilityOfFavoriteModuleInTheControlMenu() {
        checkModule.checkModuleStatus();
        if (checkModule.getModuleStatus()) {
            open(testPage);
            controlPanelRightMenu.clickPlusButton();
            controlPanelRightMenu.clickRootMenu();
            controlPanelRightMenu.clickParentMenu();
        }
        Assert.assertTrue(controlPanelRightMenu.checkSubMenuItem(Constants.SUBMENU_ITEM_NEWS),
                "Модуль '" + Constants.SUBMENU_ITEM_NEWS + "' не установлен или сломан");
    }

    @Test(description = "Добавление на тестовую страницу необходимых портлетов",
            dependsOnMethods = "checkAvailabilityOfFavoriteModuleInTheControlMenu")
    public void addTheRequiredModules() {
        open(testPage);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_NEWS);
        controlPanelRightMenu.clickPlusButton();
        controlPanelRightMenu.clickRootMenu();
        controlPanelRightMenu.clickParentMenu();
        new AddInKnowledgeModuleToThePage(Constants.SUBMENU_ITEM_NEWS, Constants.LAYOUT_COLUMN_1);
    }

    @Test(description = "Включение опции публикатора - Добавить в Избранное",
            dependsOnMethods = "addTheRequiredModules", retryAnalyzer = RunTestAgain.class)
    public void enableDraftsAndArchivesFunctions() {
        open(testPage);
        inKnowAssetPublisherConfig.openConfiguration();
        inKnowAssetPublisherConfig.enableDrafts();
        inKnowAssetPublisherConfig.enableArchives();
        inKnowAssetPublisherConfig.clickSubmitButtonAndCloseConfiguration();
    }

    @Test(description = "Создание контента через админку c одним статусом - Черновик",
            dependsOnMethods = "enableDraftsAndArchivesFunctions", retryAnalyzer = RunTestAgain.class)
    public void createContentWithOnlyDraftStatus() {
        controlPanelJournalPortletPage.openPage(testPage);
        controlPanelJournalPortletPage.addNews();
        contentNameWithDraftOnly = generateContentName();
        controlPanelJournalPortletEditContent.typeContentName(contentNameWithDraftOnly);
        controlPanelJournalPortletEditContent.clickSaveAsDraftButton();
    }

    @Test(description = "Создание контента через админку c одним статусом - Архивный",
            dependsOnMethods = "createContentWithOnlyDraftStatus", priority = 1, retryAnalyzer = RunTestAgain.class)
    public void createContentWithOnlyArchiveStatus() {
        controlPanelJournalPortletPage.openPage(testPage);
        controlPanelJournalPortletPage.addNews();
        contentNameForArchiveOnly = generateContentName();
        controlPanelJournalPortletEditContent.typeContentName(contentNameForArchiveOnly);
        controlPanelJournalPortletEditContent.clickPublishButton();
        controlPanelJournalPortletPage.makeArchiveOfContent(contentNameForArchiveOnly);
    }

    @Test(description = "Создание контента через админку c четырьмя статусами",
            dependsOnMethods = "createContentWithOnlyDraftStatus", priority = 2, retryAnalyzer = RunTestAgain.class)
    public void createContentWithAllStatuses() {
        controlPanelJournalPortletPage.openPage(testPage);
        controlPanelJournalPortletPage.addNews();
        contentNameWithAllStatuses = generateContentName();
        controlPanelJournalPortletEditContent.typeContentName(contentNameWithAllStatuses);
        controlPanelJournalPortletEditContent.clickPublishButton();
        controlPanelJournalPortletPage.makeArchiveOfContent(contentNameWithAllStatuses);
        controlPanelJournalPortletPage.editContent(contentNameWithAllStatuses);
        controlPanelJournalPortletEditContent.clickPublishButton();
        controlPanelJournalPortletPage.editContent(contentNameWithAllStatuses);
        controlPanelJournalPortletEditContent.typeNewDisplayDate(Dates.getCurrentDatePlusDays(2));
        controlPanelJournalPortletEditContent.clickPublishButton();
        controlPanelJournalPortletPage.editContent(contentNameWithAllStatuses);
        controlPanelJournalPortletEditContent.clickSaveAsDraftButton();
    }

    @Test(description = "Создание контента через админку c одним статусом - Запланировано",
            dependsOnMethods = "createContentWithOnlyDraftStatus", priority = 3, retryAnalyzer = RunTestAgain.class)
    public void createContentWithOnlyScheduledStatus() {
        controlPanelJournalPortletPage.openPage(testPage);
        controlPanelJournalPortletPage.addNews();
        contentNameWithScheduleDateOnly = generateContentName();
        controlPanelJournalPortletEditContent.typeContentName(contentNameWithScheduleDateOnly);
        controlPanelJournalPortletEditContent.typeNewDisplayDate(Dates.getCurrentDatePlusDays(2));
        controlPanelJournalPortletEditContent.clickPublishButton();
    }

    @Test(description = "Просмотр только черновиков (ожидается 2 статуса)",
            dependsOnMethods = "createContentWithOnlyDraftStatus", priority = 4, retryAnalyzer = RunTestAgain.class)
    public void checkOnlyDraftStatus() {
        open(testPage);
        inKnowAssetPublisherDraftAndArchive.clickDraftCheckboxOnThePage();
        inKnowAssetPublisherDraftAndArchive.checkStatuses(contentNameWithDraftOnly,
                Constants.WORKFLOW_STATUS_DRAFT);
    }

    @Test(description = "Просмотр только черновиков (ожидается 2 статуса)",
            dependsOnMethods = "checkOnlyDraftStatus", priority = 1, retryAnalyzer = RunTestAgain.class)
    public void checkOnlyScheduleStatus() {
        inKnowAssetPublisherDraftAndArchive.checkStatuses(contentNameWithScheduleDateOnly,
                Constants.WORKFLOW_STATUS_SCHEDULED);
    }

    @Test(description = "Просмотр только черновиков (ожидается 4 статуса)",
            dependsOnMethods = "checkOnlyDraftStatus", priority = 2, retryAnalyzer = RunTestAgain.class)
    public void checkDraftStatus() {
        inKnowAssetPublisherDraftAndArchive.checkStatuses(contentNameWithAllStatuses,
                Constants.WORKFLOW_STATUS_APPROVED,
                Constants.WORKFLOW_STATUS_DRAFT,
                Constants.WORKFLOW_STATUS_EXPIRED,
                Constants.WORKFLOW_STATUS_SCHEDULED);
    }

    @Test(description = "Просмотр черновиков и архивов (ожидается 4 статуса)",
            dependsOnMethods = "checkOnlyDraftStatus", priority = 3, retryAnalyzer = RunTestAgain.class)
    public void checkDraftAndArchiveStatuses() {
        inKnowAssetPublisherDraftAndArchive.clickArchiveCheckboxOnThePage();
        inKnowAssetPublisherDraftAndArchive.checkStatuses(contentNameWithAllStatuses,
                Constants.WORKFLOW_STATUS_APPROVED,
                Constants.WORKFLOW_STATUS_DRAFT,
                Constants.WORKFLOW_STATUS_EXPIRED,
                Constants.WORKFLOW_STATUS_SCHEDULED);
    }

    @Test(description = "Просмотр только архивов (ожидается 4 статуса)",
            dependsOnMethods = "checkOnlyDraftStatus", priority = 4, retryAnalyzer = RunTestAgain.class)
    public void checkArchiveStatus() {
        inKnowAssetPublisherDraftAndArchive.clickDraftCheckboxOnThePage();
        inKnowAssetPublisherDraftAndArchive.checkStatuses(contentNameWithAllStatuses,
                Constants.WORKFLOW_STATUS_APPROVED,
                Constants.WORKFLOW_STATUS_DRAFT,
                Constants.WORKFLOW_STATUS_EXPIRED,
                Constants.WORKFLOW_STATUS_SCHEDULED);
    }

    @Test(description = "Просмотр архива, если все версии контента в архиве (ожидается 1 статус)",
            dependsOnMethods = "checkOnlyDraftStatus", priority = 5, retryAnalyzer = RunTestAgain.class)
    public void checkOnlyArchiveStatus() {
        inKnowAssetPublisherDraftAndArchive.checkStatuses(contentNameForArchiveOnly,
                Constants.WORKFLOW_STATUS_EXPIRED);
    }

    @AfterClass(description = "Удаление модуля со стартовой страницы")
    public void removeRequiredModules() {
        refresh();
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_NEWS);
    }
}
