package ru.emdev.uitests.inknowledge.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.ReadCustomProperties;
import ru.emdev.uitests.core.RunTestAgain;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletEditContent;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletPage;

public class PredictiveSearchTest {

    private ReadCustomProperties projectProperties =
            new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    private final String testPage = projectProperties.readProperty("testPage");
    private ControlPanelJournalPortletPage controlPanelJournalPortletPage =
            new ControlPanelJournalPortletPage();
    private ControlPanelJournalPortletEditContent controlPanelJournalPortletEditContent =
            new ControlPanelJournalPortletEditContent();

    @Test(description = "Проверка предиктивного поиска",
            retryAnalyzer = RunTestAgain.class)
    public void checkPredictiveSearch() {
        controlPanelJournalPortletPage.openPage(testPage);
        controlPanelJournalPortletPage.addInknowledgeAtricle();
        controlPanelJournalPortletEditContent.typeContentBodyIntoCKEditor("/auto");

        controlPanelJournalPortletEditContent.setPredictiveContentFromList(1);

        String contentId = controlPanelJournalPortletEditContent.getPredictiveContentsList().get(0).getAttribute("data-id");
        String contentName = controlPanelJournalPortletEditContent.getPredictiveContentsList().get(0).innerText();

        Assert.assertTrue(controlPanelJournalPortletEditContent.checkPredictiveContentLink(contentId, contentName));
    }

}
