package ru.emdev.uitests.inknowledge.tests;

import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.Issue;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.AddInKnowledgeModuleToThePage;
import ru.emdev.uitests.core.CheckModule;
import ru.emdev.uitests.core.Constants;
import ru.emdev.uitests.core.portlets.ContentVersionNotePortlet;
import ru.emdev.uitests.core.utils.Dates;
import ru.emdev.uitests.core.utils.JavaScriptActions;
import ru.emdev.uitests.core.ReadCustomProperties;
import ru.emdev.uitests.core.RemoveModuleFromThePage;
import ru.emdev.uitests.core.RunTestAgain;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletEditContent;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletPage;
import ru.emdev.uitests.core.controlpanel.ControlPanelRightMenu;
import ru.emdev.uitests.core.portlets.category.manager.CategoryManagerActions;
import ru.emdev.uitests.core.portlets.category.manager.CategoryManagerConfig;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherConfig;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherContent;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

@Listeners({ ScreenShooter.class})
public class CategoryManagerTest {

    private CategoryManagerConfig categoryManagerConfig =
            new CategoryManagerConfig(Constants.SUBMENU_ITEM_CATEGORY_MANAGER);
    private InKnowAssetPublisherConfig inKnowAssetPublisherConfig =
            new InKnowAssetPublisherConfig(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
    private InKnowAssetPublisherContent inKnowAssetPublisherContent =
            new InKnowAssetPublisherContent(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
    private ControlPanelRightMenu controlPanelRightMenu =
            new ControlPanelRightMenu(Constants.ROOT_MENU_WIDGETS, Constants.PARENT_MENU_INKNOWLEDGE);
    private ControlPanelJournalPortletPage controlPanelJournalPortletPage =
            new ControlPanelJournalPortletPage();
    private ControlPanelJournalPortletEditContent controlPanelJournalPortletEditContent =
            new ControlPanelJournalPortletEditContent();
    private CheckModule checkAssetPublisherModule = new CheckModule("ru.emdev.inknowledge.asset.publisher.web");
    private CheckModule checkCategoryManagerModule = new CheckModule("ru.emdev.inknowledge.categorymanager.web");
    private ReadCustomProperties projectProperties =
            new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    private final String testPage = projectProperties.readProperty("testPage");
    private String usedContentId;
    private String vocabularyName1, vocabularyName1New, vocabularyName2;
    private String categoryName1, categoryName1New, categoryName2, categoryName3;
    private CategoryManagerActions categoryManagerTest, categoryManagerTest2;
    private List<String> contentNames;
    private boolean isAssign;

    private String generateVocabularyOrCategoryName(boolean isCategory) {
        String name;
        if (isCategory) {
            name = "Category ";
        } else {
            name = "Vocabulary ";
        }
        return name + Dates.getCurrentDateTime();
    }

    private String generateContentName() {
        return "AutoTest " + Dates.getCurrentDateTime();
    }

    @Test(description = "Убедиться, что модули 'Публикатор' и 'Менеджер категорий' активны и присутствуют в \"+\"",
            retryAnalyzer = RunTestAgain.class)
    public void checkAvailabilityOfAssetPublisherModuleInTheControlMenu() {
        checkAssetPublisherModule.checkModuleStatus();
        checkCategoryManagerModule.checkModuleStatus();
        if (checkAssetPublisherModule.getModuleStatus() && checkCategoryManagerModule.getModuleStatus()) {
            open(testPage);
            controlPanelRightMenu.clickPlusButton();
            controlPanelRightMenu.clickRootMenu();
            controlPanelRightMenu.clickParentMenu();
        }
        Assert.assertTrue(controlPanelRightMenu.checkSubMenuItem(Constants.SUBMENU_ITEM_ASSET_PUBLISHER),
                "Модуль '" + Constants.SUBMENU_ITEM_ASSET_PUBLISHER + "' не установлен или сломан");
        Assert.assertTrue(controlPanelRightMenu.checkSubMenuItem(Constants.SUBMENU_ITEM_CATEGORY_MANAGER),
                "Модуль '" + Constants.SUBMENU_ITEM_CATEGORY_MANAGER + "' не установлен или сломан");
    }

    @Test(description = "Добавление на тестовую страницу необходимых портлетов",
            dependsOnMethods = "checkAvailabilityOfAssetPublisherModuleInTheControlMenu")
    public void addTheRequiredModules() {
        open(testPage);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_CATEGORY_MANAGER);
        controlPanelRightMenu.clickPlusButton();
        controlPanelRightMenu.clickRootMenu();
        controlPanelRightMenu.clickParentMenu();
        new AddInKnowledgeModuleToThePage(Constants.SUBMENU_ITEM_ASSET_PUBLISHER, Constants.LAYOUT_COLUMN_1);
        new AddInKnowledgeModuleToThePage(Constants.SUBMENU_ITEM_CATEGORY_MANAGER, Constants.LAYOUT_COLUMN_2);
    }

    @Test(description = "Настройка портлета Публикатор",
            dependsOnMethods = "addTheRequiredModules",
            retryAnalyzer = RunTestAgain.class)
    public void prepareAssetPublisherPortlet() {
        open(testPage);
        inKnowAssetPublisherConfig.openConfiguration();
        inKnowAssetPublisherConfig.addMetadataContent(Constants.ASSET_PUBLISHER_CONFIG_METADATA_CATEGORIES_FIELD);
        inKnowAssetPublisherConfig.clickSubmitButtonAndCloseConfiguration();
    }

    @Test(description = "Настройка портлета Менеджер категорий",
            dependsOnMethods = "addTheRequiredModules",
            retryAnalyzer = RunTestAgain.class)
    public void prepareCategoryManagerPortlet() {
        open(testPage);
        categoryManagerConfig.openConfiguration();
        categoryManagerConfig.changeScope(Constants.CATEGORY_MANAGER_CONFIG_OPTION_SCOPE_SITE);
        categoryManagerConfig.changeVocabularies(Constants.CATEGORY_MANAGER_CONFIG_OPTION_ALL_DICTIONARIES);
        categoryManagerConfig.changeDisplay(Constants.CATEGORY_MANAGER_CONFIG_OPTION_DISPLAY_SIMPLE);
        categoryManagerConfig.enableEditMode();
        categoryManagerConfig.enableAppointResponsible();
        categoryManagerConfig.clickSaveButton();
    }

    @Test(description = "Создание словарей",
            dependsOnMethods = {"prepareCategoryManagerPortlet", "prepareAssetPublisherPortlet"},
            retryAnalyzer = RunTestAgain.class)
    public void createVocabularies() {
        open(testPage);
        vocabularyName1 = generateVocabularyOrCategoryName(false);
        categoryManagerTest = new CategoryManagerActions(Constants.SUBMENU_ITEM_CATEGORY_MANAGER, vocabularyName1);
        categoryManagerTest.addVocabulary(vocabularyName1, false);
        vocabularyName2 = generateVocabularyOrCategoryName(false);
        categoryManagerTest2 = new CategoryManagerActions(Constants.SUBMENU_ITEM_CATEGORY_MANAGER, vocabularyName2);
        categoryManagerTest2.addVocabulary(vocabularyName2, false);
    }

    @Test(description = "Создание категорий",
            dependsOnMethods = "createVocabularies",
            retryAnalyzer = RunTestAgain.class)
    public void createCategories() {
        open(testPage);
        categoryName1 = generateVocabularyOrCategoryName(true);
        sleep(1000);
        categoryName2 = generateVocabularyOrCategoryName(true);
        sleep(1000);
        categoryName3 = generateVocabularyOrCategoryName(true);
        categoryManagerTest.addCategory(categoryName1, categoryName1, "");
        categoryManagerTest.addCategory(categoryName2, categoryName2, "");
        categoryManagerTest2.addCategory(categoryName3, categoryName3, "");
    }

    @Test(description = "Редактирование словарей",
            dependsOnMethods = "createCategories",
            retryAnalyzer = RunTestAgain.class)
    public void editVocabularies() {
        open(testPage);
        vocabularyName1New = vocabularyName1 + " new";
        categoryManagerTest.editVocabulary(vocabularyName1New, vocabularyName1New, false);
    }

    @Test(description = "Редактирование категорий",
            dependsOnMethods = "editVocabularies",
            retryAnalyzer = RunTestAgain.class)
    public void editCategories() {
        open(testPage);
        categoryName1New = categoryName1 + " new";
        categoryManagerTest.editCategory(categoryName1, categoryName1New, categoryName1New, "");
    }

    @Test(description = "Подготовка контента для работы с категориями (создание, если требуется)",
            dependsOnMethods = "editCategories",
            retryAnalyzer = RunTestAgain.class)
    public void createContentOrGetContentId() {
        open(testPage);
        contentNames = new ArrayList<>();
        JavaScriptActions.disableAllTooltips();

        for (int i = 0; i <= 1; i++) {
            controlPanelJournalPortletPage.openPage(testPage);
            controlPanelJournalPortletPage.addInknowledgeAtricle();
            String contentName = generateContentName();
            contentNames.add(contentName);
            controlPanelJournalPortletEditContent.typeContentName(contentName);
            controlPanelJournalPortletEditContent.clickPublishButton();
            new ContentVersionNotePortlet().skipNote();
            sleep(2000); //контент не сразу появляется в портлете
        }
    }

    @Test(description = "Привязывание контента к категориям",
            dependsOnMethods = "createContentOrGetContentId",
            retryAnalyzer = RunTestAgain.class)
    public void assignContentToCategories() {
        open(testPage);
        JavaScriptActions.disableAllTooltips();
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(0));
        inKnowAssetPublisherContent.editContentById(usedContentId);
        inKnowAssetPublisherContent.assignContentToCategory(vocabularyName1New, categoryName1New);
        inKnowAssetPublisherContent.assignContentToCategory(vocabularyName1New, categoryName2);
        inKnowAssetPublisherContent.clickPublishButton();
        new ContentVersionNotePortlet().skipNote();
        isAssign = inKnowAssetPublisherContent
                .checkAssignContentToCategories(usedContentId, categoryName1New, categoryName2);
        if (!isAssign) {
            Assert.fail(
                    "Контент " + contentNames.get(0) + " не является участником категорий " + categoryName1New + " или "
                            + categoryName2);
        }

        JavaScriptActions.disableAllTooltips();
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(1));
        inKnowAssetPublisherContent.editContentById(usedContentId);
        inKnowAssetPublisherContent.assignContentToCategory(vocabularyName2, categoryName3);
        inKnowAssetPublisherContent.clickPublishButton();
        new ContentVersionNotePortlet().skipNote();
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, categoryName3);
        if (!isAssign) {
            Assert.fail("Контент " + contentNames.get(1) + " не является участником категории " + categoryName3);
        }
    }

    @Test(description = "Удаление категории с переносом контента в другую категорию",
            dependsOnMethods = "assignContentToCategories",
            alwaysRun = true,
            retryAnalyzer = RunTestAgain.class)
    public void removeCategoriesAndReplaceContent() {
        open(testPage);
        JavaScriptActions.scrollToPageOfTop();
        categoryManagerTest.removeCategoryAndReplaceContent(categoryName1New, categoryName2);
        open(testPage);
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(0));
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, categoryName2);
        if (!isAssign) {
            Assert.fail("Контент " + contentNames.get(0) + " не является участником категории " + categoryName2);
        }
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, categoryName1New);
        if (isAssign) {
            Assert.fail("Контент " + contentNames.get(0) + " не должен являться участником удалённой категории "
                    + categoryName1New);
        }
    }

    @Test(description = "Удаление словаря с переносом контента в другой словарь",
            dependsOnMethods = "assignContentToCategories",
            priority = 1,
            alwaysRun = true,
            retryAnalyzer = RunTestAgain.class)
    public void removeVocabulariesAndReplaceContent() {
        open(testPage);
        JavaScriptActions.scrollToPageOfTop();
        categoryManagerTest.removeVocabularyWithReplaceCategories(vocabularyName2);
        open(testPage);
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(0));
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, categoryName2);
        if (!isAssign) {
            Assert.fail("Контент " + contentNames.get(0) + " не является участником категории " + categoryName2);
        }
        usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentNames.get(1));
        isAssign = inKnowAssetPublisherContent.checkAssignContentToCategories(usedContentId, categoryName3);
        if (!isAssign) {
            Assert.fail("Контент " + contentNames.get(1) + " не является участником категории " + categoryName3);
        }
    }

    @Test(description = "Удаление словаря со всеми категориями и контентом",
            dependsOnMethods = "assignContentToCategories",
            priority = 2,
            alwaysRun = true)
    @Issue("INKNOW-135")
    public void removeDefaultVocabularyWithAllContent() {
        open(testPage);
        JavaScriptActions.scrollToPageOfTop();
        categoryManagerTest2.removeVocabularyWithAllCategories();
        open(testPage);
        sleep(3000);
        for (String contentName : contentNames) {
            for (String contentId : inKnowAssetPublisherContent.getContentIds()) {
                if (inKnowAssetPublisherContent.getContentNameById(contentId).contains(contentName)) {
                    Assert.fail("Контент " + contentName + " не удалился после удаления словаря \"" + vocabularyName2
                            + "\" с категорями");
                }
            }
        }
    }

    @AfterClass(description = "Удаление модулей со стартовой страницы")
    public void removeRequiredModules() {
        open(testPage);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
        new RemoveModuleFromThePage(Constants.SUBMENU_ITEM_CATEGORY_MANAGER);
    }
}
