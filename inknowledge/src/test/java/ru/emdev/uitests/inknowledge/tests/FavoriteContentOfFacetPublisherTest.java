package ru.emdev.uitests.inknowledge.tests;

import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.Issue;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.Constants;
import ru.emdev.uitests.core.ReadCustomProperties;
import ru.emdev.uitests.core.RunTestAgain;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletEditContent;
import ru.emdev.uitests.core.controlpanel.ControlPanelJournalPortletPage;
import ru.emdev.uitests.core.portlets.ContentVersionNotePortlet;
import ru.emdev.uitests.core.portlets.favorites.InKnowFavorites;
import ru.emdev.uitests.core.portlets.favorites.InKnowFavoritesContent;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherContent;
import ru.emdev.uitests.core.portlets.publisher.InKnowAssetPublisherFavorites;
import ru.emdev.uitests.core.utils.Dates;
import ru.emdev.uitests.core.utils.JavaScriptActions;

import static com.codeborne.selenide.Selenide.*;

@Listeners({ScreenShooter.class})
public class FavoriteContentOfFacetPublisherTest {

    private ReadCustomProperties projectProperties =
            new ReadCustomProperties("src/test/resources/configs/portalAuth.properties");
    private final String testPage = projectProperties.readProperty("testPageOfFavoritesFacetPublisher");
    private InKnowAssetPublisherContent inKnowAssetPublisherContent =
            new InKnowAssetPublisherContent(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
    private InKnowAssetPublisherFavorites inKnowAssetPublisherFavorites =
            new InKnowAssetPublisherFavorites(Constants.SUBMENU_ITEM_ASSET_PUBLISHER);
    private InKnowFavorites inKnowFavorites =
            new InKnowFavorites(Constants.SUBMENU_ITEM_FAVORITES);
    private ControlPanelJournalPortletPage controlPanelJournalPortletPage =
            new ControlPanelJournalPortletPage();
    private ControlPanelJournalPortletEditContent controlPanelJournalPortletEditContent =
            new ControlPanelJournalPortletEditContent();
    private InKnowFavoritesContent inKnowFavoritesContent =
            new InKnowFavoritesContent(Constants.SUBMENU_ITEM_FAVORITES);

    private String generateContentName() {
        return "ArticleInKnow " + Dates.getCurrentDateTime();
    }

    private void createNewsContent(String contentName) {
        controlPanelJournalPortletPage.openPage(testPage);
        controlPanelJournalPortletPage.addInknowledgeAtricle();
        controlPanelJournalPortletEditContent.typeContentName(contentName);
        controlPanelJournalPortletEditContent.clickPublishButton();
        new ContentVersionNotePortlet().skipNote();
        sleep(3000); //контент в портлете появляется с задержкой
    }

    @Test(description = "Добавление контента в избранное",
            retryAnalyzer = RunTestAgain.class)
    public void addToFavoriteFromAssetPublisherAndDeleteFromAssetPublisher() {
        String contentName = generateContentName();
        createNewsContent(contentName);

        open(testPage);
        JavaScriptActions.disableAllTooltips();

        String usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentName);
        inKnowAssetPublisherFavorites.clickAddContentToTheFavoriteById(usedContentId, false);
        Assert.assertTrue(inKnowFavorites.checkContentStatusInTheFavoriteById(usedContentId),
                "Контент " + usedContentId + " не добавлен в избранное");

        sleep(1000);

        inKnowAssetPublisherFavorites.clickRemoveContentFromTheFavoriteById(usedContentId, false);
        Assert.assertFalse(inKnowFavorites.checkContentStatusInTheFavoriteById(usedContentId),
                "Контент " + usedContentId + " не удалён из избранного при удалении через Публикатор");
    }

    // Проверяем, что искомый контент отсутствует в портлете Избранное (INKNOW-47)
    @Issue("INKNOW-47")
    @Test(description = "Удаление контента из Избранного через Избранное",
            retryAnalyzer = RunTestAgain.class)
    public void addToFavoriteFromAssetPublisherAndDeleteFromFavorites() {
        String contentName = generateContentName();
        createNewsContent(contentName);

        open(testPage);
        JavaScriptActions.disableAllTooltips();

        String usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentName);
        inKnowAssetPublisherFavorites.clickAddContentToTheFavoriteById(usedContentId, false);
        Assert.assertTrue(inKnowFavorites.checkContentStatusInTheFavoriteById(usedContentId),
                "Контент  " + usedContentId + " не добавлен в избранное");

        sleep(1000);

        inKnowFavorites.clickRemoveContentFromTheFavoriteById(usedContentId);
        Assert.assertFalse(inKnowFavorites.checkContentStatusInTheFavoriteById(usedContentId),
                "Контент " + usedContentId + " не удалён при удалении из избранного через портлет Избранное");

        for (int i = 0; i < inKnowFavoritesContent.getAllContentNames().size(); i++) {
            if (inKnowFavoritesContent.getAllContentNames().get(i).text().trim()
                    .equals(contentName)) {
                Assert.fail("После нажатия на звёздочку в Избранном контент " + usedContentId + " остался в избранном");
            }
        }
    }

    @Issue("INKNOW-111")
    @Test(description = "Добавление открытого контента в Избранное и его удаление",
            retryAnalyzer = RunTestAgain.class)
    public void addOpenedContentToTheFavoriteAndRemoveFromOpenedContent() {
        String contentName = generateContentName();
        createNewsContent(contentName);

        open(testPage);
        JavaScriptActions.disableAllTooltips();

        String usedContentId = inKnowAssetPublisherContent.getContentIdByName(contentName);
        inKnowAssetPublisherContent.openContent(usedContentId);
        inKnowAssetPublisherFavorites.clickAddContentToTheFavoriteById(usedContentId,false);
        Assert.assertTrue(inKnowAssetPublisherFavorites.checkAddContentToFavoriteStatusById(usedContentId, false),
                "Проверка, что контент уже в Избранном, провалилась");
        refresh();
        Assert.assertTrue(inKnowFavorites.checkContentStatusInTheFavoriteById(usedContentId),
                "Контент не добавлен в избранное");
        open(testPage);
        inKnowAssetPublisherContent.openContent(usedContentId);
        inKnowAssetPublisherFavorites.clickRemoveContentFromTheFavoriteById(usedContentId,false);
        refresh();
        Assert.assertFalse(inKnowFavorites.checkContentStatusInTheFavoriteById(usedContentId),
                "Контент " + usedContentId + " не удалён при удалении из избранного через портлет Избранное");
    }

}
