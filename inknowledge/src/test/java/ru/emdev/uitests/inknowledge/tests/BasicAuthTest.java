package ru.emdev.uitests.inknowledge.tests;

import com.codeborne.selenide.*;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.ITestContext;
import org.testng.annotations.*;
import ru.emdev.uitests.core.auth.BasicAuth;

@Listeners({ ScreenShooter.class})
public class BasicAuthTest {

    private BasicAuth basicAuth;

    public BasicAuthTest() {
        basicAuth = new BasicAuth("src/test/resources/configs/basicAuth.properties");
    }

    @BeforeSuite(description = "Определение парметров перед запуском тестов")
    public void setupBeforeSuite(ITestContext context) {
        Configuration.baseUrl = basicAuth.getBasicAuthUrl();
        Configuration.reportsFolder = "target/allure-results";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
//        Configuration.holdBrowserOpen = true;

        if (WebDriverRunner.isChrome()) {
            System.setProperty("chromeoptions.args", "--no-sandbox" +
                    ",--disable-gpu," +
                    "enable-automation" +
                    "--disable-infobars" +
                    "--disable-dev-shm-usage" +
                    "--disable-browser-side-navigation");
        }

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

    @Test(description = "Базовая авторизация на портале")
    public void basicAuth() {
        basicAuth.runBasicAuthorization();
    }

    @AfterSuite(description = "Удаление слушателя")
    public void setupAfterSuite() {
        SelenideLogger.removeListener("AllureSelenide");
    }
}
