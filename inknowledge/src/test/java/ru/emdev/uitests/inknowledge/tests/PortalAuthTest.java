package ru.emdev.uitests.inknowledge.tests;

import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.testng.ScreenShooter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.RunTestAgain;
import ru.emdev.uitests.core.auth.PortalAuthPage;

@Listeners({ ScreenShooter.class})
public class PortalAuthTest {

    private PortalAuthPage portalAuthPage = new PortalAuthPage();

    @Test(description = "Авторизация на портале", retryAnalyzer = RunTestAgain.class)
    public void portalAuth() {
        WebDriverRunner.clearBrowserCache();
        portalAuthPage.auth();
    }
}
