package ru.emdev.uitests.inknowledge.tests;

import com.jcraft.jsch.JSchException;
import org.testng.annotations.Test;
import ru.emdev.uitests.core.ReadCustomProperties;
import ru.emdev.uitests.core.utils.mail.SmtpServer;
import ru.emdev.uitests.core.utils.ssh.ReverseSSHPortForwarding;

import static com.codeborne.selenide.Selenide.sleep;

public class MailCatchingTest {

    private ReadCustomProperties customProperties =
            new ReadCustomProperties("src/test/resources/configs/sshAuth.properties");
    private SmtpServer smtpServer = new SmtpServer();
    private ReverseSSHPortForwarding reverseSSHPortForwarding;

    @Test//(dependsOnMethods = "startSMTPServer")
    public void openReverseSSHPortForwarding() throws JSchException {
        reverseSSHPortForwarding = new ReverseSSHPortForwarding(customProperties, true);
        reverseSSHPortForwarding.open();
    }

    @Test(dependsOnMethods = "openReverseSSHPortForwarding")
    public void startSMTPServer() throws Exception {
        smtpServer.startSmtpServer(SmtpServer.CUSTOM_SMTP_PORT);
    }

    @Test(dependsOnMethods = "startSMTPServer")
    public void mailCatching() {
        sleep(10000);
        for (int i = 0; i < smtpServer.getReceivedEmails().size(); i++) {
            System.out.println("============================");

            System.out.println(smtpServer.getReceivedEmails().get(i).getBody());

            System.out.println("============================");
        };
    }

    @Test(dependsOnMethods = "mailCatching", alwaysRun = true)
    public void stopSMTPServer() {
        smtpServer.stopSmtpServer();
    }

    @Test(dependsOnMethods = "stopSMTPServer")
    public void closeReverseSSHPortForwarding() throws JSchException {
        reverseSSHPortForwarding.close();
    }
}
